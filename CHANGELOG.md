# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## v 1.0.0 - 2019-01-04
### Added 
- support for daylight saving and time zone

### Changed
- the numeric value of January (JAN) is 1 now

### Fixed
- month part range was wrong, range must be 1 to 12
