Master Branch: [![pipeline status](https://gitlab.com/schedule4j/schedule4j-cron/badges/master/pipeline.svg)](https://gitlab.com/schedule4j/schedule4j-cron/commits/master) [![coverage report](https://gitlab.com/schedule4j/schedule4j-cron/badges/master/coverage.svg)](https://gitlab.com/schedule4j/schedule4j-cron/commits/master)

Develop Branch: [![pipeline status](https://gitlab.com/schedule4j/schedule4j-cron/badges/develop/pipeline.svg)](https://gitlab.com/schedule4j/schedule4j-cron/commits/develop) [![coverage report](https://gitlab.com/schedule4j/schedule4j-cron/badges/develop/coverage.svg)](https://gitlab.com/schedule4j/schedule4j-cron/commits/develop)

This project contains classes for parse cron expressions and calculate time points from a cron expression.
