/*
 * Created on 2011-04-10
 *
 * Copyright 2011 Dirk Buchhorn.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron;

import io.gitlab.schedule4j.cron.part.CronPart;
import io.gitlab.schedule4j.cron.part.DayOfMonthPart;
import io.gitlab.schedule4j.cron.part.DayOfWeekPart;
import io.gitlab.schedule4j.cron.part.HourPart;
import io.gitlab.schedule4j.cron.part.MinutePart;
import io.gitlab.schedule4j.cron.part.MonthPart;
import io.gitlab.schedule4j.cron.part.SecondPart;
import io.gitlab.schedule4j.cron.part.YearPart;
import io.gitlab.schedule4j.cron.subpart.AllNumbersSubpart;
import io.gitlab.schedule4j.cron.subpart.CronSubpart;
import io.gitlab.schedule4j.cron.subpart.GroupSubpart;
import io.gitlab.schedule4j.cron.subpart.LastDayOfMonthSubpart;
import io.gitlab.schedule4j.cron.subpart.LastWeekdayOfMonthSubpart;
import io.gitlab.schedule4j.cron.subpart.LastXthDayOfWeekSubpart;
import io.gitlab.schedule4j.cron.subpart.NearestWeekdayOfMonthSubpart;
import io.gitlab.schedule4j.cron.subpart.NumbersSubpart;
import io.gitlab.schedule4j.cron.subpart.RangeNumbersSubpart;
import io.gitlab.schedule4j.cron.subpart.XthDayOfMonthSubpart;

import java.text.ParseException;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Dirk Buchhorn
 */
class CronExpressionParser
{
	private static final String MONTH_NAMES = "JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC";
	private static final String WEEKDAY_NAMES = "SUN|MON|TUE|WED|THU|FRI|SAT";
	private static final Pattern REPLACE_MONTH_VALUES_PATTERN = Pattern.compile(MONTH_NAMES);
	private static final Pattern REPLACE_DAY_OF_WEEK_VALUES_PATTERN = Pattern.compile(WEEKDAY_NAMES);
	private static final Map<String, String> REPLACE_MONTH_VALUES_MAP;
	private static final Map<String, String> REPLACE_DAY_OF_WEEK_VALUES_MAP;
	private static final Pattern STANDARD_PATTERN = Pattern
		.compile("(\\d+|\\*)-(\\d+|\\*)/(\\d+)|(\\d+|\\*)-(\\d+|\\*)()|(\\d+|\\*)()/(\\d+)|(\\d+)()()");
	private static final Pattern DAY_OF_WEEK_PATTERN = Pattern.compile("([1-7])#([1-5])");
	private static final Pattern LAST_DAY_OF_WEEK_PATTERN = Pattern.compile("([1-7])L");
	private static final Pattern WEEKDAY_OF_MONTH_PATTERN = Pattern.compile("([1-9]|1[0-9]|2[0-9]|3[0-1])W");

	static
	{
		Map<String, String> map = new HashMap<String, String>();
		String[] split = MONTH_NAMES.split("\\|");
		for (int i = 0; i < split.length; i++)
		{
			map.put(split[i], Integer.toString(i + 1));
		}
		REPLACE_MONTH_VALUES_MAP = Collections.unmodifiableMap(map);
		map = new HashMap<String, String>();
		split = WEEKDAY_NAMES.split("\\|");
		for (int i = 0; i < split.length; i++)
		{
			map.put(split[i], Integer.toString(i + 1));
		}
		REPLACE_DAY_OF_WEEK_VALUES_MAP = Collections.unmodifiableMap(map);
	}

	private CronExpressionParser()
	{
	}

	protected static CronSubpart parseSecondSubpart(String part) throws ParseException
	{
		return parseSubpart(part, ChronoField.SECOND_OF_MINUTE, 0, 59);
	}

	protected static CronSubpart parseMinuteSubpart(String part) throws ParseException
	{
		return parseSubpart(part, ChronoField.MINUTE_OF_HOUR, 0, 59);
	}

	protected static CronSubpart parseHourOfDaySubpart(String part) throws ParseException
	{
		return parseSubpart(part, ChronoField.HOUR_OF_DAY, 0, 23);
	}

	protected static CronSubpart parseDayOfMonthSubpart(String part) throws ParseException
	{
		return parseSubpart(part, ChronoField.DAY_OF_MONTH, 1, 31);
	}

	protected static CronSubpart parseMonthSubpart(String part) throws ParseException
	{
		return parseSubpart(part, ChronoField.MONTH_OF_YEAR, 1, 12);
	}

	protected static CronSubpart parseDayOfWeekSubpart(String part) throws ParseException
	{
		return parseSubpart(part, ChronoField.DAY_OF_WEEK, 1, 7);
	}

	protected static CronSubpart parseYearSubpart(String part) throws ParseException
	{
		return parseSubpart(part, ChronoField.YEAR, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}

	protected static List<CronPart> parse(String cronExpression) throws ParseException
	{
		List<CronPart> cronParts = new ArrayList<CronPart>();
		if (cronExpression == null || cronExpression.length() == 0)
		{
			throw new IllegalArgumentException("Cron expression can't be null or empty!");
		}
		// correct the cronExpression
		String correctedCronExpression = cronExpression.trim().replaceAll(" +", " ");
		String[] split = correctedCronExpression.trim().split(" ");
		if (split.length < 6 || split.length > 7)
		{
			throw new ParseException("Invalid cron expression '" + cronExpression + "'! Found " + split.length
				+ " cron expression parts. But a cron expression must have 6 or 7 parts.", -1);
		}
		// parse second part
		SecondPart secondPart = new SecondPart(parseSecondSubpart(split[0]));

		// parse minute part
		MinutePart minutePart = new MinutePart(parseMinuteSubpart(split[1]));

		// parse hour part
		HourPart hourPart = new HourPart(parseHourOfDaySubpart(split[2]));

		// parse day of month part
		CronSubpart dayOfMonthSubpart = parseDayOfMonthSubpart(split[3]);

		// parse month part
		MonthPart monthPart = new MonthPart(parseMonthSubpart(split[4]));

		// parse day of week part
		CronSubpart dayOfWeekSubpart = parseDayOfWeekSubpart(split[5]);

		cronParts.add(secondPart);
		cronParts.add(minutePart);
		cronParts.add(hourPart);
		if (dayOfMonthSubpart == null && dayOfWeekSubpart == null)
		{
			throw new ParseException("One of the fields 'day of month' and 'day of week' must be specified! "
				+ "Not both can be set to '?'!", -1);
		}
		else if (dayOfMonthSubpart != null && dayOfWeekSubpart != null)
		{
			throw new ParseException("Only one of the fields 'day of month' and 'day of week' can be specified! "
				+ "One must be set to '?'!", -1);
		}
		if (dayOfMonthSubpart != null)
		{
			cronParts.add(new DayOfMonthPart(dayOfMonthSubpart));
		}
		else
		{
			cronParts.add(new DayOfWeekPart(dayOfWeekSubpart));
		}
		cronParts.add(monthPart);

		// parse year part
		if (split.length == 7 && !split[6].trim().equals("*"))
		{
			cronParts.add(new YearPart(parseYearSubpart(split[6])));
		}
		return cronParts;
	}

	protected static CronSubpart parseSubpart(String part, ChronoField chronoField, int minValue, int maxValue)
		throws ParseException
	{
		if ((ChronoField.DAY_OF_MONTH == chronoField || ChronoField.DAY_OF_WEEK == chronoField) && part.equals("?"))
		{
			return null;
		}
		boolean isDayOfMonth = chronoField == ChronoField.DAY_OF_MONTH;
		boolean isDayOfWeek = chronoField == ChronoField.DAY_OF_WEEK;

		String[] subparts = part.split(",");
		GroupSubpart groupSubpart = new GroupSubpart();
		for (int i = 0; i < subparts.length; i++)
		{
			if (subparts[i].length() > 0)
			{
				if (subparts[i].equals("*"))
				{
					groupSubpart.add(new AllNumbersSubpart(chronoField));
				}
				else
				{
					String subpart = subparts[i];// .replaceAll("\\*", "0");
					subpart = subpart.toUpperCase();
					if (isDayOfWeek)
					{
						subpart = replaceDayOfWeekValues(subpart);
					}
					else if (chronoField == ChronoField.MONTH_OF_YEAR)
					{
						subpart = replaceMonthValues(subpart);
					}

					Matcher standardMatcher = STANDARD_PATTERN.matcher(subpart);
					Matcher dayOfWeekMatcher = DAY_OF_WEEK_PATTERN.matcher(subpart);
					Matcher lastDayOfWeekMatcher = LAST_DAY_OF_WEEK_PATTERN.matcher(subpart);
					Matcher weekdayOfMonthMatcher = WEEKDAY_OF_MONTH_PATTERN.matcher(subpart);
					if (standardMatcher.matches())
					{
						int startValue = minValue;
						int endValue = -1;
						int increment = -1;

						for (int k = 1; k <= standardMatcher.groupCount(); k++)
						{
							String value = standardMatcher.group(k);
							if (value != null && value.length() > 0)
							{
								if (value.equals("*"))
								{
									switch (k % 3)
									{
										case 0:
											// can't occur
											break;
										case 1:
											startValue = minValue;
											break;
										case 2:
											endValue = maxValue;
									}
								}
								else
								{
									switch (k % 3)
									{
										case 0:
											increment = Integer.parseInt(value);
											break;
										case 1:
											startValue = Integer.parseInt(value);
											break;
										case 2:
											endValue = Integer.parseInt(value);
									}
								}
							}
						}
						if (startValue > maxValue)
						{
							throw new ParseException("Wrong cron expression subpart '" + subparts[i] + "'! Value '"
								+ startValue + "' is to large!", -1);
						}
						if (startValue < minValue)
						{
							throw new ParseException("Wrong cron expression subpart '" + subparts[i] + "'! Value '"
								+ startValue + "' is to small!", -1);
						}
						if (endValue > maxValue)
						{
							throw new ParseException("Wrong cron expression subpart '" + subparts[i] + "'! Value '" + endValue
								+ "' is to large!", -1);
						}
						if (endValue >= 0 && endValue < minValue)
						{
							throw new ParseException("Wrong cron expression subpart '" + subparts[i] + "'! Value '" + endValue
								+ "' is to small!", -1);
						}
						if (increment == 0)
						{
							throw new ParseException("Wrong cron expression subpart '" + subparts[i]
								+ "'! Increment must be greater than 0!", -1);
						}

						if ((endValue == -1 && increment == -1) || startValue == endValue)
						{
							// only a single number
							if (isDayOfWeek)
							{
								startValue = correctWeekdayValue(startValue);
							}
							groupSubpart.add(new NumbersSubpart(chronoField, startValue));
						}
						else
						{
							if (isDayOfWeek)
							{
								startValue = correctWeekdayValue(startValue);
								endValue = correctWeekdayValue(endValue);
							}
							endValue = endValue < 0 ? maxValue : endValue;
							increment = increment > 0 ? increment : 1;
							groupSubpart.add(new RangeNumbersSubpart(chronoField, startValue, endValue, increment));
						}
					}
					else if (isDayOfMonth && subpart.equals("L"))
					{
						groupSubpart.add(new LastDayOfMonthSubpart());
					}
					else if (isDayOfMonth && subpart.equals("LW"))
					{
						groupSubpart.add(new LastWeekdayOfMonthSubpart());
					}
					else if (isDayOfMonth && weekdayOfMonthMatcher.matches())
					{
						groupSubpart.add(new NearestWeekdayOfMonthSubpart(Integer.parseInt(weekdayOfMonthMatcher.group(1))));
					}
					else if (isDayOfWeek && subpart.equals("L"))
					{
						groupSubpart.add(new LastXthDayOfWeekSubpart(0));
					}
					else if (isDayOfWeek && dayOfWeekMatcher.matches())
					{
						int weekday = correctWeekdayValue(Integer.parseInt(dayOfWeekMatcher.group(1)));
						int count = Integer.parseInt(dayOfWeekMatcher.group(2));
						groupSubpart.add(new XthDayOfMonthSubpart(weekday, count));
					}
					else if (isDayOfWeek && lastDayOfWeekMatcher.matches())
					{
						int weekday = correctWeekdayValue(Integer.parseInt(lastDayOfWeekMatcher.group(1)));
						groupSubpart.add(new LastXthDayOfWeekSubpart(weekday));
					}
					else
					{
						throw new ParseException("Wrong cron expression subpart '" + subparts[i] + "'!", -1);
					}
				}
			}
		}

		CronSubpart cronSubpart = groupSubpart;
		if (groupSubpart.size() == 0)
		{
			throw new ParseException("Wrong cron expression part '" + part + "'!", -1);
		}
		if (groupSubpart.size() == 1)
		{
			cronSubpart = groupSubpart.getCronParts().get(0);
		}
		return cronSubpart;
	}

	protected static String replaceMonthValues(String subpart)
	{
		return replaceValues(subpart, REPLACE_MONTH_VALUES_PATTERN, REPLACE_MONTH_VALUES_MAP);
	}

	protected static String replaceDayOfWeekValues(String subpart)
	{
		return replaceValues(subpart, REPLACE_DAY_OF_WEEK_VALUES_PATTERN, REPLACE_DAY_OF_WEEK_VALUES_MAP);
	}

	protected static String replaceValues(String subpart, Pattern pattern, Map<String, String> map)
	{
		Matcher m = pattern.matcher(subpart);
		StringBuffer sb = new StringBuffer();
		while (m.find())
		{
			m.appendReplacement(sb, map.get(m.group()));
		}
		m.appendTail(sb);
		return sb.toString();
	}

	protected static int correctWeekdayValue(int weekdayValue)
	{
		int correctedValue = weekdayValue - 1;
		if (correctedValue < 1)
		{
			correctedValue = 7;
		}
		return correctedValue;
	}
}
