/*
 * Created on 2010-03-20
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.part.DayOfWeekPart;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjusters;

/**
 * @author Dirk Buchhorn
 */
public abstract class AbstractCalendarFieldSubpart implements CronSubpart
{
	private ChronoField chronoField;

	public AbstractCalendarFieldSubpart(ChronoField chronoField)
	{
		this.chronoField = chronoField;
	}

	protected ChronoField getChronoField()
	{
		return chronoField;
	}

	public boolean isRotatingNumbers()
	{
		return chronoField != ChronoField.YEAR;
	}

	protected int getMinValue(ZonedDateTime dateTime)
	{
		int minValue = 0;
		switch (chronoField)
		{
			case SECOND_OF_MINUTE:
			case MINUTE_OF_HOUR:
			case HOUR_OF_DAY:
				minValue = 0;
				break;
			case DAY_OF_MONTH:
			case DAY_OF_WEEK:
			case MONTH_OF_YEAR:
				minValue = 1;
				break;
			case YEAR:
				minValue = Integer.MIN_VALUE;
				break;
			default:
				throw new UnsupportedOperationException("ChronoField '" + chronoField.name() + "' is not supported");
		}
		return minValue;
	}

	protected int getMaxValue(ZonedDateTime dateTime)
	{
		int maxValue = 0;
		switch (chronoField)
		{
			case SECOND_OF_MINUTE:
			case MINUTE_OF_HOUR:
				maxValue = 59;
				break;
			case HOUR_OF_DAY:
				maxValue = 23;
				break;
			case DAY_OF_MONTH:
				maxValue = dateTime.with(TemporalAdjusters.lastDayOfMonth()).get(ChronoField.DAY_OF_MONTH);
				break;
			case DAY_OF_WEEK:
				maxValue = 7;
				break;
			case MONTH_OF_YEAR:
				maxValue = 12;
				break;
			case YEAR:
				maxValue = Integer.MAX_VALUE;
				break;
			default:
				throw new UnsupportedOperationException("ChronoField '" + chronoField.name() + "' is not supported");
		}
		return maxValue;
	}

	@Override
	public boolean check(ZonedDateTime dateTime)
	{
		int minValue = getMinValue(dateTime);
		int maxValue = getMaxValue(dateTime);
		return check(dateTime.get(chronoField), minValue, maxValue);
	}

	public abstract boolean check(int value, int minValue, int maxValue);

	@Override
	public final CronResult getNext(ZonedDateTime dateTime)
	{
		CronResult cronResult = null;
		int minValue = getMinValue(dateTime);
		int maxValue = getMaxValue(dateTime);
		int usedMaxValue = maxValue;
		if (chronoField == ChronoField.DAY_OF_MONTH)
		{
			// set the maximum to 31, we check the result value later
			usedMaxValue = 31;
		}
		cronResult = getNext(dateTime.get(chronoField), minValue, usedMaxValue);
		if (chronoField == ChronoField.DAY_OF_WEEK)
		{
			cronResult = DayOfWeekPart.correctToDayOfMonth(cronResult, dateTime);
		}
		else if (chronoField == ChronoField.DAY_OF_MONTH)
		{
			// check the result value if it is greater than the maxValue or we have an addCarry
			// save the addCarry
			if (cronResult.getAddCarry() != 0)
			{
				if (cronResult.getNumber() > 28)
				{
					// check the maximum of another month
					ZonedDateTime dt = dateTime.withDayOfMonth(1).plusMonths(cronResult.getAddCarry());
					maxValue = getMaxValue(dt);
					if (cronResult.getNumber() > maxValue)
					{
						// calculate a next result
						// save the addCarray
						int addCarry = cronResult.getAddCarry();
						dt = dateTime.withDayOfMonth(maxValue);
						cronResult = getNext(31, minValue, 31);
						addCarry += cronResult.getAddCarry();
						cronResult.setAddCarry(addCarry);
					}
				}
			}
			else if (cronResult.getNumber() > maxValue)
			{
				// calculate a next result
				// we don't have to save the addCarry because we don't have one
				cronResult = getNext(31, minValue, 31);
			}
		}
		return cronResult;
	}

	public abstract CronResult getNext(int value, int minValue, int maxValue);

	@Override
	public final CronResult getPrevious(ZonedDateTime dateTime)
	{
		CronResult cronResult = null;
		int minValue = getMinValue(dateTime);
		int maxValue = getMaxValue(dateTime);
		int usedMaxValue = maxValue;
		if (chronoField == ChronoField.DAY_OF_MONTH)
		{
			// set the maximum to 31, we check the result value later
			usedMaxValue = 31;
		}
		cronResult = getPrevious(dateTime.get(chronoField), minValue, usedMaxValue);
		if (chronoField == ChronoField.DAY_OF_WEEK)
		{
			cronResult = DayOfWeekPart.correctToDayOfMonth(cronResult, dateTime);
		}
		else if (chronoField == ChronoField.DAY_OF_MONTH)
		{
			// check the result value if it is greater than the maxValue or we have an addCarry
			// save the addCarry
			if (cronResult.getAddCarry() != 0)
			{
				if (cronResult.getNumber() > 28)
				{
					// check the maximum of another month
					ZonedDateTime dt = dateTime.withDayOfMonth(1).plusMonths(cronResult.getAddCarry());
					maxValue = getMaxValue(dt);
					if (cronResult.getNumber() > maxValue)
					{
						// calculate a next result
						// save the addCarray
						int addCarry = cronResult.getAddCarry();
						dt = dt.withDayOfMonth(maxValue);
						cronResult = getPrevious(maxValue + 1, minValue, 31);
						addCarry += cronResult.getAddCarry();
						cronResult.setAddCarry(addCarry);
					}
				}
			}
		}
		return cronResult;
	}

	public abstract CronResult getPrevious(int value, int minValue, int maxValue);
}
