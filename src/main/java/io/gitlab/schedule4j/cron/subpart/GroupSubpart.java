/*
 * Created on 2010-02-20
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import io.gitlab.schedule4j.cron.CronResult;

import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Dirk Buchhorn
 */
public class GroupSubpart implements CronSubpart
{
	private List<CronSubpart> parts = new LinkedList<CronSubpart>();

	public GroupSubpart()
	{
	}

	public void add(CronSubpart cronPart)
	{
		if (cronPart.getClass().equals(NumbersSubpart.class))
		{
			// optimized add
			NumbersSubpart subpart = null;
			for (CronSubpart part : parts)
			{
				if (part instanceof NumbersSubpart)
				{
					subpart = (NumbersSubpart) part;
					break;
				}
			}
			if (subpart != null)
			{
				subpart.addNumbers((NumbersSubpart) cronPart);
			}
			else
			{
				parts.add(cronPart);
			}
		}
		else
		{
			parts.add(cronPart);
		}
	}

	public List<CronSubpart> getCronParts()
	{
		return parts;
	}

	@Override
	public boolean check(ZonedDateTime dateTime)
	{
		for (CronSubpart cronPart : parts)
		{
			if (cronPart.check(dateTime))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public CronResult getNext(ZonedDateTime dateTime)
	{
		CronResult cronResult = null;
		for (CronSubpart cronPart : parts)
		{
			CronResult nextNumber = cronPart.getNext(dateTime);
			if (cronResult == null || nextNumber.before(cronResult))
			{
				cronResult = nextNumber;
			}
		}
		return cronResult;
	}

	@Override
	public CronResult getPrevious(ZonedDateTime dateTime)
	{
		CronResult cronResult = null;
		for (CronSubpart cronPart : parts)
		{
			CronResult previousNumber = cronPart.getPrevious(dateTime);
			if (cronResult == null || previousNumber.after(cronResult))
			{
				cronResult = previousNumber;
			}
		}
		return cronResult;
	}

	public int size()
	{
		return parts.size();
	}
}