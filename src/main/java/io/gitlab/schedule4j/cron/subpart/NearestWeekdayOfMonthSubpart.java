/*
 * Created on 2011-04-11
 *
 * Copyright 2011 Dirk Buchhorn.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import java.time.DayOfWeek;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;

/**
 * This class represents the 'xxW' in a day-of-month cron part.
 * 
 * @author Dirk Buchhorn
 */
public class NearestWeekdayOfMonthSubpart extends AbstractDayOfMonthSubpart
{
	private int dayOfMonth;

	public NearestWeekdayOfMonthSubpart(int dayOfMonth)
	{
		if (dayOfMonth > 31 || dayOfMonth < 1)
		{
			throw new IllegalArgumentException(
				"The weekDayOfMonth value is incorrect! " + "(0<" + dayOfMonth + "<=31)");
		}
		this.dayOfMonth = dayOfMonth;
	}

	@Override
	protected int calculateDayOfMonth(ZonedDateTime dateTime, int monthOffset)
	{
		ZonedDateTime dt = dateTime.with(TemporalAdjusters.firstDayOfMonth()).plusMonths(monthOffset)
			.with(TemporalAdjusters.lastDayOfMonth());
		int maxValue = dt.getDayOfMonth();
		int usedDayOfMonth = dayOfMonth > maxValue ? maxValue : dayOfMonth;
		dt = dt.withDayOfMonth(usedDayOfMonth);
		DayOfWeek dayOfWeek = dt.getDayOfWeek();
		int calculatedDayOfMonth = usedDayOfMonth;
		if (dayOfWeek == DayOfWeek.SATURDAY)
		{
			if (calculatedDayOfMonth == 1)
			{
				// use Monday
				calculatedDayOfMonth += 2;
			}
			else
			{
				// use Friday
				calculatedDayOfMonth--;
			}
		}
		else if (dayOfWeek == DayOfWeek.SUNDAY)
		{
			if (calculatedDayOfMonth == maxValue)
			{
				// use Friday
				calculatedDayOfMonth -= 2;
			}
			else
			{
				// use Monday
				calculatedDayOfMonth++;
			}
		}
		return calculatedDayOfMonth;
	}
}
