/*
 * Created on 2010-02-28
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import io.gitlab.schedule4j.cron.CronResult;

import java.time.ZonedDateTime;

/**
 * A cron part can be split into 1 to n sub parts. Every sub part must implement this interface.
 * 
 * @author Dirk Buchhorn
 */
public interface CronSubpart
{
	public boolean check(ZonedDateTime dateTime);

	/**
	 * Calculates the next valid number.
	 * 
	 * @param dateTime the dateTime for calculation
	 * @return a {@link CronResult}
	 */
	public CronResult getNext(ZonedDateTime dateTime);

	/**
	 * Calculates the previous valid number.
	 * 
	 * @param dateTime the dateTime for calculation
	 * @return a {@link CronResult}
	 */
	public CronResult getPrevious(ZonedDateTime dateTime);
}
