/*
 * Created on 2011-04-12
 *
 * Copyright 2011 Dirk Buchhorn.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import io.gitlab.schedule4j.cron.CronResult;

import java.time.ZonedDateTime;

/**
 * @author Dirk Buchhorn
 */
public abstract class AbstractDayOfMonthSubpart implements CronSubpart
{
	@Override
	public boolean check(ZonedDateTime dateTime)
	{
		boolean check = false;
		int currentDay = dateTime.getDayOfMonth();
		int day = calculateDayOfMonth(dateTime, 0);
		check = day == currentDay;
		return check;
	}

	protected abstract int calculateDayOfMonth(ZonedDateTime dateTime, int monthOffset);

	@Override
	public CronResult getNext(ZonedDateTime dateTime)
	{
		CronResult cronResult = new CronResult();
		int currentDay = dateTime.getDayOfMonth();
		int offset = 0;
		int day = 0;
		while ((day = calculateDayOfMonth(dateTime, offset)) < 0 || (offset == 0 && currentDay >= day))
		{
			offset++;
		}
		cronResult.setNumber(day);
		cronResult.setAddCarry(offset);
		return cronResult;
	}

	@Override
	public CronResult getPrevious(ZonedDateTime dateTime)
	{
		CronResult cronResult = new CronResult();
		int currentDay = dateTime.getDayOfMonth();
		int offset = 0;
		int day = 0;
		while ((day = calculateDayOfMonth(dateTime, offset)) < 0 || (offset == 0 && currentDay <= day))
		{
			offset--;
		}
		cronResult.setNumber(day);
		cronResult.setAddCarry(offset);
		return cronResult;
	}
}
