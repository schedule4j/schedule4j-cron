/*
 * Created on 2010-03-03
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import io.gitlab.schedule4j.cron.CronResult;

import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;

/**
 * This class represents the 'L' in a day-of-month cron part.
 * 
 * @author Dirk Buchhorn
 */
public class LastDayOfMonthSubpart implements CronSubpart
{
	public LastDayOfMonthSubpart()
	{
	}

	@Override
	public boolean check(ZonedDateTime dateTime)
	{
		boolean check = false;
		ZonedDateTime dt = dateTime.with(TemporalAdjusters.lastDayOfMonth());
		int lastDayOfMonth = dt.getDayOfMonth();
		check = lastDayOfMonth == dateTime.getDayOfMonth();
		return check;
	}

	@Override
	public CronResult getNext(ZonedDateTime dateTime)
	{
		CronResult cronResult = new CronResult();
		int maxValue = dateTime.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth();
		if (dateTime.getDayOfMonth() < maxValue)
		{
			cronResult.setNumber(maxValue);
		}
		else
		{
			ZonedDateTime dt = dateTime.with(TemporalAdjusters.firstDayOfMonth()).plusMonths(1)
				.with(TemporalAdjusters.lastDayOfMonth());
			int lastDayOfNextMonth = dt.getDayOfMonth();
			cronResult.setNumber(lastDayOfNextMonth);
			cronResult.setAddCarry(1);
		}
		return cronResult;
	}

	@Override
	public CronResult getPrevious(ZonedDateTime dateTime)
	{
		CronResult cronResult = new CronResult();
		ZonedDateTime dt = dateTime.with(TemporalAdjusters.firstDayOfMonth()).plusMonths(-1)
			.with(TemporalAdjusters.lastDayOfMonth());
		int lastDayOfNextMonth = dt.getDayOfMonth();
		cronResult.setNumber(lastDayOfNextMonth);
		cronResult.setAddCarry(-1);
		return cronResult;
	}
}
