/*
 * Created on 2010-03-03
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import io.gitlab.schedule4j.cron.CronResult;

import java.time.DayOfWeek;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;

/**
 * This class represents the '[1-7]L' in a day-of-week cron part.
 * 
 * @author Dirk Buchhorn
 */
public class LastXthDayOfWeekSubpart implements CronSubpart
{
	private int dayOfWeek;

	public LastXthDayOfWeekSubpart(int dayOfWeek)
	{
		this.dayOfWeek = dayOfWeek;
	}

	@Override
	public boolean check(ZonedDateTime dateTime)
	{
		int lastWeekdayOfMonth = calculateLastXthDayOfWeek(dateTime, 0);
		boolean check = lastWeekdayOfMonth == dateTime.getDayOfMonth();
		return check;
	}

	@Override
	public CronResult getNext(ZonedDateTime dateTime)
	{
		CronResult cronResult = new CronResult();
		int value = dateTime.getDayOfMonth();
		int lastXthDayOfWeek = calculateLastXthDayOfWeek(dateTime, 0);
		if (value < lastXthDayOfWeek)
		{
			cronResult.setNumber(lastXthDayOfWeek);
		}
		else
		{
			lastXthDayOfWeek = calculateLastXthDayOfWeek(dateTime, 1);
			cronResult.setNumber(lastXthDayOfWeek);
			cronResult.setAddCarry(1);
		}
		return cronResult;
	}

	private int calculateLastXthDayOfWeek(ZonedDateTime dateTime, int monthOffset)
	{
		ZonedDateTime dt = dateTime.with(TemporalAdjusters.firstDayOfMonth()).plusMonths(monthOffset)
			.with(TemporalAdjusters.lastDayOfMonth());
		int maxValue = dt.getDayOfMonth();
		DayOfWeek dayOfWeekOfTheLastDay = dt.getDayOfWeek();
		if (dayOfWeekOfTheLastDay.getValue() == dayOfWeek)
		{
			return maxValue;
		}
		else if (dayOfWeekOfTheLastDay.getValue() > dayOfWeek)
		{
			return maxValue - (dayOfWeekOfTheLastDay.getValue() - dayOfWeek);
		}
		else
		{
			return maxValue - (7 - dayOfWeek) - dayOfWeekOfTheLastDay.getValue();
		}
	}

	@Override
	public CronResult getPrevious(ZonedDateTime dateTime)
	{
		CronResult cronResult = new CronResult();
		int value = dateTime.getDayOfMonth();
		int lastXthDayOfWeek = calculateLastXthDayOfWeek(dateTime, 0);
		if (value > lastXthDayOfWeek)
		{
			cronResult.setNumber(lastXthDayOfWeek);
		}
		else
		{
			lastXthDayOfWeek = calculateLastXthDayOfWeek(dateTime, -1);
			cronResult.setNumber(lastXthDayOfWeek);
			cronResult.setAddCarry(-1);
		}
		return cronResult;
	}
}
