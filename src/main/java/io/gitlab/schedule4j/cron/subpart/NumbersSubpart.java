/*
 * Created on 2010-02-20
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import io.gitlab.schedule4j.cron.CronResult;

import java.time.temporal.ChronoField;
import java.util.NavigableSet;
import java.util.TreeSet;

/**
 * This class represents a single number in a cron part.
 * 
 * @author Dirk Buchhorn
 */
public class NumbersSubpart extends AbstractCalendarFieldSubpart
{
	private NavigableSet<Integer> allowedNumbers = new TreeSet<Integer>();

	public NumbersSubpart(ChronoField chronoField, int number)
	{
		super(chronoField);
		addNumber(number);
	}

	protected NavigableSet<Integer> getAllowedNumbers()
	{
		return allowedNumbers;
	}

	public void addNumber(int number)
	{
		allowedNumbers.add(number);
	}

	public void addNumbers(NumbersSubpart subpart)
	{
		allowedNumbers.addAll(subpart.allowedNumbers);
	}

	@Override
	public boolean check(int value, int minValue, int maxValue)
	{
		boolean check = false;
		if (minValue <= value && value <= maxValue)
		{
			check = allowedNumbers.contains(value);
		}
		return check;
	}

	@Override
	public CronResult getNext(int value, int minValue, int maxValue)
	{
		Integer next = allowedNumbers.higher(value);
		if (next == null && !isRotatingNumbers())
		{
			return null;
		}
		CronResult cronResult = new CronResult();
		if (next == null || next > maxValue)
		{
			next = allowedNumbers.ceiling(minValue);
			cronResult.setAddCarry(1);
		}
		next = Math.max(next, minValue);
		cronResult.setNumber(next);
		return cronResult;
	}

	@Override
	public CronResult getPrevious(int value, int minValue, int maxValue)
	{
		Integer previous = allowedNumbers.lower(value);
		if (previous == null && !isRotatingNumbers())
		{
			return null;
		}
		CronResult cronResult = new CronResult();
		if (previous == null || previous < minValue)
		{
			previous = allowedNumbers.floor(maxValue);
			cronResult.setAddCarry(-1);
		}
		previous = Math.min(previous, maxValue);
		cronResult.setNumber(previous);
		return cronResult;
	}
}