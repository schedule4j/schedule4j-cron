/*
 * Created on 2015-07-19
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import io.gitlab.schedule4j.cron.CronResult;

import java.time.DayOfWeek;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;

/**
 * This class represents the 'LW' in a day-of-month cron part.
 * 
 * @author Dirk Buchhorn
 */
public class LastWeekdayOfMonthSubpart implements CronSubpart
{

	private int calculateLastWeekdayOfMonth(ZonedDateTime dateTime, int monthOffset)
	{
		ZonedDateTime dt = dateTime.with(TemporalAdjusters.firstDayOfMonth()).plusMonths(monthOffset)
			.with(TemporalAdjusters.lastDayOfMonth());
		int dayOfMonth = dt.getDayOfMonth();
		DayOfWeek dayOfWeek = dt.getDayOfWeek();
		if (dayOfWeek == DayOfWeek.SATURDAY)
		{
			dayOfMonth--;
		}
		else if (dayOfWeek == DayOfWeek.SUNDAY)
		{
			dayOfMonth -= 2;
		}
		return dayOfMonth;
	}

	@Override
	public boolean check(ZonedDateTime dateTime)
	{
		int lastWeekdayOfMonth = calculateLastWeekdayOfMonth(dateTime, 0);
		boolean check = lastWeekdayOfMonth == dateTime.getDayOfMonth();
		return check;
	}

	@Override
	public CronResult getNext(ZonedDateTime dateTime)
	{
		CronResult cronResult = new CronResult();
		int value = dateTime.getDayOfMonth();
		int lastWeekdayOfMonth = calculateLastWeekdayOfMonth(dateTime, 0);
		if (value < lastWeekdayOfMonth)
		{
			cronResult.setNumber(lastWeekdayOfMonth);
		}
		else
		{
			lastWeekdayOfMonth = calculateLastWeekdayOfMonth(dateTime, 1);
			cronResult.setNumber(lastWeekdayOfMonth);
			cronResult.setAddCarry(1);
		}
		return cronResult;
	}

	@Override
	public CronResult getPrevious(ZonedDateTime dateTime)
	{
		CronResult cronResult = new CronResult();
		int value = dateTime.getDayOfMonth();
		int lastWeekdayOfMonth = calculateLastWeekdayOfMonth(dateTime, 0);
		if (value > lastWeekdayOfMonth)
		{
			cronResult.setNumber(lastWeekdayOfMonth);
		}
		else
		{
			lastWeekdayOfMonth = calculateLastWeekdayOfMonth(dateTime, -1);
			cronResult.setNumber(lastWeekdayOfMonth);
			cronResult.setAddCarry(-1);
		}
		return cronResult;
	}
}
