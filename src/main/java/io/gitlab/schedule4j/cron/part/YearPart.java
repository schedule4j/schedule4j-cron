/*
 * Created on 2010-02-28
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.part;

import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.subpart.CronSubpart;

import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;

/**
 * @author Dirk Buchhorn
 */
public class YearPart extends CronPart
{

	public YearPart(CronSubpart cronSubpart)
	{
		super(cronSubpart);
	}

	@Override
	public ZonedDateTime calculateNext(ZonedDateTime dateTime)
	{
		CronResult cronResult = getCronSubpart().getNext(dateTime);
		if (cronResult == null)
		{
			return null;
		}
		ZonedDateTime dt = dateTime.with(TemporalAdjusters.firstDayOfMonth()).withYear(cronResult.getNumber())
			.withMonth(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
		return dt;
	}

	@Override
	public ZonedDateTime calculatePrevious(ZonedDateTime dateTime)
	{
		CronResult cronResult = getCronSubpart().getPrevious(dateTime);
		if (cronResult == null)
		{
			return null;
		}
		ZonedDateTime dt = dateTime.with(TemporalAdjusters.firstDayOfMonth()).withYear(cronResult.getNumber())
			.withMonth(12).with(TemporalAdjusters.lastDayOfMonth()).withHour(23).withMinute(59).withSecond(59)
			.withNano(0);
		return dt;
	}
}
