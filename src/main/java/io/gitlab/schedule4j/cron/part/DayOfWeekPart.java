/*
 * Created on 2010-02-28
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.part;

import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.subpart.CronSubpart;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjusters;

/**
 * @author Dirk Buchhorn
 */
public class DayOfWeekPart extends DayOfMonthPart
{
	public DayOfWeekPart(CronSubpart cronSubpart)
	{
		super(cronSubpart);
	}

	/*
	 * Helper method
	 */
	public static CronResult correctToDayOfMonth(CronResult cronResult, ZonedDateTime dateTime)
	{
		CronResult result = new CronResult();
		int maxValue = dateTime.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth();
		int value = dateTime.getDayOfMonth();
		int newValue = value - dateTime.get(ChronoField.DAY_OF_WEEK) + cronResult.getNumber() + 7
			* cronResult.getAddCarry();
		if (newValue > maxValue)
		{
			result.setAddCarry(1);
			result.setNumber(newValue - maxValue);
		}
		else if (newValue < 1)
		{
			ZonedDateTime dt = dateTime.withDayOfMonth(1).minusMonths(1);
			maxValue = dt.with(TemporalAdjusters.lastDayOfMonth()).get(ChronoField.DAY_OF_MONTH);
			result.setAddCarry(-1);
			result.setNumber(maxValue + newValue);
		}
		else
		{
			result.setNumber(newValue);
		}
		return result;
	}
}
