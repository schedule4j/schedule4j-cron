/*
 * Created on 2010-02-28
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.part;

import io.gitlab.schedule4j.cron.subpart.CronSubpart;

import java.time.ZonedDateTime;

/**
 * A cron expression can be split into 6 to 7 parts (second, minute, hour, day-of-month, month, day-of-week
 * [and year]). This class is the abstract super class of all part classes.
 * 
 * @author Dirk Buchhorn
 */
public abstract class CronPart
{
	private CronSubpart cronSubpart;

	public CronPart(CronSubpart cronSubpart)
	{
		this.cronSubpart = cronSubpart;
	}

	public CronSubpart getCronSubpart()
	{
		return cronSubpart;
	}

	/**
	 * Checks if the dateTime value is valid for this part.
	 * 
	 * @param dateTime the calendar for calculation
	 * @return true if the calendar value is valid for this part
	 */
	public boolean check(ZonedDateTime dateTime)
	{
		return cronSubpart.check(dateTime);
	}

	/**
	 * Calculates the next valid value. Return null if no value could be calculated.
	 * 
	 * @param dateTime the dateTime for calculation
	 * @return the next value or null if no next value could be calculated
	 */
	public abstract ZonedDateTime calculateNext(ZonedDateTime dateTime);

	/**
	 * Calculates the previous valid value. Return null if no value could be calculated.
	 * 
	 * @param dateTime the dateTime for calculation
	 * @return the previous value or null if no previous value could be calculated
	 */
	public abstract ZonedDateTime calculatePrevious(ZonedDateTime dateTime);
}
