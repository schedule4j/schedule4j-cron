/*
 * Created on 2010-02-28
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.part;

import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.subpart.CronSubpart;

import java.time.ZonedDateTime;

/**
 * @author Dirk Buchhorn
 */
public class MinutePart extends CronPart
{

	public MinutePart(CronSubpart cronSubpart)
	{
		super(cronSubpart);
	}

	@Override
	public ZonedDateTime calculateNext(ZonedDateTime dateTime)
	{
		CronResult cronResult = getCronSubpart().getNext(dateTime);
		ZonedDateTime dt = dateTime.withMinute(cronResult.getNumber()).withSecond(0).withNano(0);
		if (cronResult.getAddCarry() != 0)
		{
			dt = dt.plusHours(cronResult.getAddCarry());
		}
		return dt;
	}

	@Override
	public ZonedDateTime calculatePrevious(ZonedDateTime dateTime)
	{
		CronResult cronResult = getCronSubpart().getPrevious(dateTime);
		ZonedDateTime dt = dateTime.withMinute(cronResult.getNumber()).withSecond(59).withNano(0);
		if (cronResult.getAddCarry() != 0)
		{
			dt = dt.plusHours(cronResult.getAddCarry());
		}
		return dt;
	}
}
