/*
 * Created on 2015-06-24
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import io.gitlab.schedule4j.cron.part.CronPart;

/**
 * @author Dirk Buchhorn
 */
public class CronExpression
{
	public static final int DEFAULT_INFINITE_LOOP_DETECTION_VALUE = 100;

	private String cronExpression;
	private int infiniteLoopDetectionValue = DEFAULT_INFINITE_LOOP_DETECTION_VALUE;

	private List<CronPart> cronParts = new ArrayList<CronPart>();

	/**
	 * Creates a new {@link CronExpression} with the given cron expression.
	 * 
	 * @param cronExpression the cron expression
	 * @throws ParseException if the cron expression could not be parsed
	 */
	public CronExpression(String cronExpression) throws ParseException
	{
		this.cronExpression = cronExpression;
		cronParts = CronExpressionParser.parse(cronExpression);
	}

	/**
	 * An infinite loop is detected based on the year change count during the calculation.
	 * 
	 * @return the infinite loop detection value
	 */
	public int getInfiniteLoopDetectionValue()
	{
		return infiniteLoopDetectionValue;
	}

	/**
	 * Sets the infinite loop detection value.
	 * 
	 * @param infiniteLoopDetectionValue the infinite loop detection value
	 */
	public void setInfiniteLoopDetectionValue(int infiniteLoopDetectionValue)
	{
		this.infiniteLoopDetectionValue = infiniteLoopDetectionValue;
	}

	/**
	 * Gets the string representation of this cron expression.
	 * 
	 * @return the string representation of this cron expression
	 */
	public String getCronExpression()
	{
		return cronExpression;
	}

	public boolean isSatisfiedBy(ZonedDateTime dateTime)
	{
		for (int i = 0; i < cronParts.size(); i++)
		{
			CronPart cronPart = cronParts.get(i);
			if (!cronPart.check(dateTime))
			{
				return false;
			}
		}
		return true;
	}

	public ZonedDateTime getNextTime(ZonedDateTime dateTime)
	{
		TimeCalculator tc = new TimeCalculator();
		return tc.getNextTime(dateTime);
	}

	public ZonedDateTime getPreviousTime(ZonedDateTime dateTime)
	{
		TimeCalculator tc = new TimeCalculator();
		return tc.getPreviousTime(dateTime);
	}

	private class TimeCalculator
	{
		private int lastYear = 0;
		private int yearChangeCount = 0;
		ZonedDateTime dt = null;
		private int i;

		private ZonedDateTime getNextTime(ZonedDateTime dateTime)
		{
			if (dateTime == null)
			{
				return null;
			}
			// we must count the year changes, so we can detect a no next year match and prevent an infinite loop
			yearChangeCount = 0;
			ZonedDateTime nextDateTime = null;
			// set the date to the next valid second
			dt = cronParts.get(0).calculateNext(dateTime);
			lastYear = dt.getYear();
			// iterate from year to second is the best one (most effective one)
			for (i = cronParts.size() - 1; i >= 0; i--)
			{
				CronPart cronPart = cronParts.get(i);
				if (!cronPart.check(dt))
				{
					// same code start
					sameCodeGetNext(cronPart);
					if (dt == null)
					{
						return null;
					}
					// same code end
					for (i = i + 1; i < cronParts.size(); i++)
					{
						cronPart = cronParts.get(i);
						if (!cronPart.check(dt))
						{
							// same code start
							sameCodeGetNext(cronPart);
							if (dt == null)
							{
								return null;
							}
							// same code end
						}
					}
				}
				nextDateTime = dt;
			}
			return nextDateTime;
		}

		private void sameCodeGetNext(CronPart cronPart)
		{
			dt = cronPart.calculateNext(dt);
			// dt can be null when the year part is given (like '2010-2015' or '2014,2015')
			if (dt != null && lastYear != dt.getYear())
			{
				lastYear = dt.getYear();
				yearChangeCount++;
				if (yearChangeCount > infiniteLoopDetectionValue)
				{
					dt = null;
				}
			}
		}

		private ZonedDateTime getPreviousTime(ZonedDateTime dateTime)
		{
			if (dateTime == null)
			{
				return null;
			}
			// we must count the year changes, so we can detect a no previous year match and prevent an infinite
			// loop
			yearChangeCount = 0;
			ZonedDateTime previousDateTime = null;
			// set the date to the previous valid second
			dt = cronParts.get(0).calculatePrevious(dateTime);
			lastYear = dt.getYear();
			// iterate from year to second is the best one (most effective one)
			for (int i = cronParts.size() - 1; i >= 0; i--)
			{
				CronPart cronPart = cronParts.get(i);
				if (!cronPart.check(dt))
				{
					// same code start
					sameCodeGetPrevious(cronPart);
					if (dt == null)
					{
						return null;
					}
					// same code end
					for (i = i + 1; i < cronParts.size(); i++)
					{
						cronPart = cronParts.get(i);
						if (!cronPart.check(dt))
						{
							// same code start
							sameCodeGetPrevious(cronPart);
							if (dt == null)
							{
								return null;
							}
							// same code end
						}
					}
				}
				previousDateTime = dt;
			}
			return previousDateTime;
		}

		private void sameCodeGetPrevious(CronPart cronPart)
		{
			dt = cronPart.calculatePrevious(dt);
			if (dt != null && lastYear != dt.getYear())
			{
				lastYear = dt.getYear();
				yearChangeCount++;
				if (yearChangeCount > infiniteLoopDetectionValue)
				{
					dt = null;
				}
			}
		}
	}
}
