/*
 * Created on 2010-02-20
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron;

/**
 * @author Dirk Buchhorn
 */
public class CronResult
{
	private int number;
	private int addCarry;

	public CronResult()
	{
	}

	public CronResult(int number, int addCarry)
	{
		super();
		this.number = number;
		this.addCarry = addCarry;
	}

	public int getNumber()
	{
		return number;
	}

	public void setNumber(int number)
	{
		this.number = number;
	}

	public int getAddCarry()
	{
		return addCarry;
	}

	public void setAddCarry(int addCarry)
	{
		this.addCarry = addCarry;
	}

	public boolean after(CronResult cronValue)
	{
		boolean after = false;
		if ((number > cronValue.number && addCarry == cronValue.addCarry) || (addCarry > cronValue.addCarry))
		{
			after = true;
		}
		return after;
	}

	public boolean before(CronResult cronValue)
	{
		boolean before = false;
		if ((number < cronValue.number && addCarry == cronValue.addCarry) || (addCarry < cronValue.addCarry))
		{
			before = true;
		}
		return before;
	}

	@Override
	public String toString()
	{
		return "CronResult [number=" + number + ", addCarry=" + addCarry + "]";
	}
}
