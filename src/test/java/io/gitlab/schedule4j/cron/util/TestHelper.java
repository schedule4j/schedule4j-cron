/*
 * Created on 2015-11-18
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Dirk Buchhorn
 */
public class TestHelper
{
	private TestHelper()
	{
	}

	public static ZonedDateTime createZonedDateTimeByDateTimeStr(String dateTimeStr)
	{
		ZonedDateTime dateTime = LocalDateTime.parse(dateTimeStr, DateTimeFormatter.ISO_DATE_TIME).atZone(
			ZoneId.of("Europe/Berlin"));
		return dateTime;
	}

	public static ZonedDateTime createZonedDateTimeByDateStr(String dateStr)
	{
		ZonedDateTime dateTime = LocalDate.parse(dateStr, DateTimeFormatter.ISO_DATE).atStartOfDay(
			ZoneId.of("Europe/Berlin"));
		return dateTime;
	}
}
