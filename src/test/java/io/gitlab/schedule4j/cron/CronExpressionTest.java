/*
 * Created on 2015-07-15
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.junit.Test;

import io.gitlab.schedule4j.cron.util.TestHelper;

/**
 * @author Dirk Buchhorn
 */
public class CronExpressionTest
{
	@Test
	public void testCronExpressions() throws ParseException
	{
		CronExpression cronExpression = new CronExpression("0-30/15,30-0/10 * * * * ?");
		ZonedDateTime dateTime = cronExpression.getNextTime(createZonedDateTime("2000-02-01T12:09:00"));
		assertEquals(createZonedDateTime("2000-02-01T12:09:15"), dateTime);
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-02-01T12:31:32"));
		assertEquals(createZonedDateTime("2000-02-01T12:31:40"), dateTime);

		cronExpression = new CronExpression("0 0/8,0/5 * * * ?");
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-02-01T12:09:00"));
		assertEquals(createZonedDateTime("2000-02-01T12:10:00"), dateTime);
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-02-01T12:31:00"));
		assertEquals(createZonedDateTime("2000-02-01T12:32:00"), dateTime);

		cronExpression = new CronExpression("0 0 5-10,0/9 * * ?");
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-02-01T09:00:00"));
		assertEquals(createZonedDateTime("2000-02-01T10:00:00"), dateTime);
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-02-01T12:00:00"));
		assertEquals(createZonedDateTime("2000-02-01T18:00:00"), dateTime);

		cronExpression = new CronExpression("0 0 0 5-10,1/8 * ?");
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-02-01T09:00:00"));
		assertEquals(createZonedDateTime("2000-02-05T00:00:00"), dateTime);
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-02-11T12:00:00"));
		assertEquals(createZonedDateTime("2000-02-17T00:00:00"), dateTime);

		cronExpression = new CronExpression("0 0 0 1 3-6 ?");
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-02-01T00:00:00"));
		assertEquals(createZonedDateTime("2000-03-01T00:00:00"), dateTime);
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-03-11T12:00:00"));
		assertEquals(createZonedDateTime("2000-04-01T00:00:00"), dateTime);

		cronExpression = new CronExpression("0 0 0 ? 2 1#1,5L");
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-01-01T00:00:00"));
		assertEquals(createZonedDateTime("2000-02-06T00:00:00"), dateTime);
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-02-11T12:00:00"));
		assertEquals(createZonedDateTime("2000-02-24T00:00:00"), dateTime);

		cronExpression = new CronExpression("0 0 0 8W * ?");
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-01-01T00:00:00"));
		assertEquals(createZonedDateTime("2000-01-07T00:00:00"), dateTime);
		dateTime = cronExpression.getNextTime(createZonedDateTime("2000-02-11T12:00:00"));
		assertEquals(createZonedDateTime("2000-03-08T00:00:00"), dateTime);
	}

	@Test
	public void getSetInfiniteLoopDetectionValueTest() throws ParseException
	{
		CronExpression cronExpression = new CronExpression("0 * * * * ?");
		cronExpression.setInfiniteLoopDetectionValue(10);
		assertEquals(10, cronExpression.getInfiniteLoopDetectionValue());
	}

	@Test
	public void getCronExpressionTest() throws ParseException
	{
		String cron = "0 * * * * ?";
		CronExpression cronExpression = new CronExpression(cron);
		assertEquals(cron, cronExpression.getCronExpression());
	}

	@Test
	public void isSatisfiedByTest() throws ParseException
	{
		CronExpression cronExpression = new CronExpression("0 * * * * ?");
		ZonedDateTime zdt = ZonedDateTime.now().truncatedTo(ChronoUnit.MINUTES);
		assertTrue(cronExpression.isSatisfiedBy(zdt));
		zdt = zdt.plusSeconds(1);
		assertFalse(cronExpression.isSatisfiedBy(zdt));
	}

	@Test
	public void getNextTimeTest() throws ParseException
	{
		CronExpression cronExpression = new CronExpression("0 0/10 2-10 * * ?");
		assertNull(cronExpression.getNextTime(null));
		assertEquals(createZonedDateTime("2000-01-01T02:00:00"),
			cronExpression.getNextTime(createZonedDateTime("2000-01-01T00:01:00")));
		assertEquals(createZonedDateTime("2000-01-02T02:00:00"),
			cronExpression.getNextTime(createZonedDateTime("2000-01-01T10:51:00")));

		cronExpression = new CronExpression("0 0/10 2-10 * * ? 2010");
		assertEquals(createZonedDateTime("2010-01-01T02:00:00"),
			cronExpression.getNextTime(createZonedDateTime("2000-01-01T00:01:00")));
		// test 'if (dt != null && lastYear != dt.getYear())'
		assertNull(cronExpression.getNextTime(createZonedDateTime("2010-12-31T23:59:59")));

		// infinite loop detection test
		cronExpression = new CronExpression("0 0 * 29 2 ? 2009/4");
		assertNull(cronExpression.getNextTime(createZonedDateTime("2010-01-01T02:00:00")));
	}

	@Test
	public void getPreviousTimeTest() throws ParseException
	{
		CronExpression cronExpression = new CronExpression("0 0/10 2-10 * * ?");
		assertNull(cronExpression.getPreviousTime(null));
		assertEquals(createZonedDateTime("2000-01-01T10:50:00"),
			cronExpression.getPreviousTime(createZonedDateTime("2000-01-02T00:00:00")));
		assertEquals(createZonedDateTime("2000-01-01T09:50:00"),
			cronExpression.getPreviousTime(createZonedDateTime("2000-01-01T10:00:00")));

		cronExpression = new CronExpression("0 0/10 2-10 * * ? 2010");
		assertEquals(createZonedDateTime("2010-01-01T10:50:00"),
			cronExpression.getPreviousTime(createZonedDateTime("2010-01-02T00:01:00")));
		// test 'if (dt != null && lastYear != dt.getYear())'
		assertNull(cronExpression.getPreviousTime(createZonedDateTime("2010-01-01T00:00:00")));

		// infinite loop detection test
		cronExpression = new CronExpression("0 0 * 29 2 ? 2009/4");
		assertNull(cronExpression.getPreviousTime(createZonedDateTime("2500-01-01T02:00:00")));
	}

	@Test
	public void testDaylightSavings() throws ParseException
	{
		// last Sunday in march and October
		CronExpression cronExpression = new CronExpression("0 0/30 1-4 ? MAR,OCT SUNL");
		ZonedDateTime dateTime = cronExpression.getNextTime(createZonedDateTime("2015-02-01T00:00:00"));
		assertEquals(createZonedDateTime("2015-03-29T01:00:00"), dateTime);
		dateTime = cronExpression.getNextTime(dateTime);
		assertEquals(createZonedDateTime("2015-03-29T01:30:00"), dateTime);
		// switch to the next hour
		dateTime = cronExpression.getNextTime(dateTime);
		assertEquals(createZonedDateTime("2015-03-29T03:00:00"), dateTime);

		dateTime = cronExpression.getNextTime(createZonedDateTime("2015-07-01T00:00:00"));
		assertEquals(createZonedDateTime("2015-10-25T01:00:00"), dateTime);
		dateTime = cronExpression.getNextTime(dateTime);
		assertEquals(createZonedDateTime("2015-10-25T01:30:00"), dateTime);
		dateTime = cronExpression.getNextTime(dateTime);
		ZonedDateTime dt = ZonedDateTime.parse("2015-10-25T02:00:00+02:00[Europe/Berlin]");
		assertEquals(dt, dateTime);
		dateTime = cronExpression.getNextTime(dateTime);
		dt = dt.plusMinutes(30);
		assertEquals(dt, dateTime);
		dateTime = cronExpression.getNextTime(dateTime);
		dt = dt.plusMinutes(30);
		assertEquals(dt, dateTime);
		dateTime = cronExpression.getNextTime(dateTime);
		dt = dt.plusMinutes(30);
		assertEquals(dt, dateTime);
	}

	// ///
	// helper methods
	// ///

	/**
	 * Util method to create a new Date by adding an offset to a given Date.
	 * 
	 * @param date an arbitrary date
	 * @param offset The offset to <tt>date</tt>. If this offset is negative, the resulting date is before
	 *        <tt>date</tt>
	 * @return a new Date
	 */
	protected Date calculateTime(Date date, long offset)
	{
		return new Date(date.getTime() + offset);
	}

	/**
	 * Utility method to create a new {@link ZonedDateTime} from a given date formated string.
	 * 
	 * @param str the date formated string
	 * @return a new Date
	 */
	protected ZonedDateTime createZonedDateTime(String dateTimeStr)
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateTimeStr(dateTimeStr);
		return dateTime;
	}
}
