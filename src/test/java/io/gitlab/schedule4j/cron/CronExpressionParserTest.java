/*
 * Created on 2011-04-10
 *
 * Copyright 2011 Dirk Buchhorn.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import io.gitlab.schedule4j.cron.subpart.AllNumbersSubpart;
import io.gitlab.schedule4j.cron.subpart.CronSubpart;
import io.gitlab.schedule4j.cron.subpart.GroupSubpart;
import io.gitlab.schedule4j.cron.subpart.NumbersSubpart;
import io.gitlab.schedule4j.cron.subpart.RangeNumbersSubpart;

import java.text.ParseException;
import java.time.temporal.ChronoField;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class CronExpressionParserTest
{
	@Test
	public void parseTest() throws ParseException
	{
		try
		{
			CronExpressionParser.parse(null);
			fail("No IllegalArgumentException was thrown, because of cron expression was null");
		}
		catch (IllegalArgumentException e)
		{}
		try
		{
			CronExpressionParser.parse("");
			fail("No IllegalArgumentException was thrown, because of cron expression was empty");
		}
		catch (IllegalArgumentException e)
		{}
		try
		{
			CronExpressionParser.parse("0");
			fail("No ParseException was thrown, because of cron expression must have at least 6 parts");
		}
		catch (ParseException e)
		{}
		try
		{
			CronExpressionParser.parse("0 0 0 * *");
			fail("No ParseException was thrown, because of cron expression must have at least 6 parts");
		}
		catch (ParseException e)
		{}
		try
		{
			CronExpressionParser.parse("0 0 0 * * ? * *");
			fail("No ParseException was thrown, because of cron expression has too much parts");
		}
		catch (ParseException e)
		{}
		try
		{
			CronExpressionParser.parse("0 0 0 ? * ?");
			fail("No ParseException was thrown, because only dayOfMonth or dayOfWeek can be set to '?'");
		}
		catch (ParseException e)
		{}
		try
		{
			CronExpressionParser.parse("0 0 0 * * *");
			fail("No ParseException was thrown, because only one of the fields 'day of month' and 'day of week' can be specified");
		}
		catch (ParseException e)
		{}
		CronExpressionParser.parse("0 0 0 * * ?");
		CronExpressionParser.parse("0 0 0 ? * * *");
		CronExpressionParser.parse("0 0 0 ? * * *");
		CronExpressionParser.parse("0 0 0 ? * * 2000-2020");
	}

	@Test
	public void parseSubpartFailTest()
	{
		try
		{
			CronExpressionParser.parseSubpart("", ChronoField.SECOND_OF_MINUTE, 0, 59);
			fail("No ParseException was thrown, because cron part was wrong");
		}
		catch (ParseException e)
		{}
		try
		{
			CronExpressionParser.parseSubpart(",", ChronoField.SECOND_OF_MINUTE, 0, 59);
			fail("No ParseException was thrown, because cron part was wrong");
		}
		catch (ParseException e)
		{}
		try
		{
			CronExpressionParser.parseSubpart("20-60", ChronoField.SECOND_OF_MINUTE, 0, 59);
			fail("No ParseException was thrown, end value is greater than the max value");
		}
		catch (ParseException e)
		{}
		try
		{
			CronExpressionParser.parseSubpart("8-0", ChronoField.MONTH_OF_YEAR, 1, 12);
			fail("No ParseException was thrown, end value is less than the min value");
		}
		catch (ParseException e)
		{}
		try
		{
			CronExpressionParser.parseSubpart("10-40/0", ChronoField.SECOND_OF_MINUTE, 0, 59);
			fail("No ParseException was thrown, increment can't be null");
		}
		catch (ParseException e)
		{}
	}

	@Test
	public void parseSecondPartTest() throws ParseException
	{
		basicParsePartTests(ChronoField.SECOND_OF_MINUTE, 0, 59);
		CronExpressionParser.parseSecondSubpart("5,1-5,1/3");
	}

	@Test
	public void parseMinutePartTest() throws ParseException
	{
		basicParsePartTests(ChronoField.MINUTE_OF_HOUR, 0, 59);
		CronExpressionParser.parseMinuteSubpart("5,1-5,1/3");
	}

	@Test
	public void parseHourOfDayPartTest() throws ParseException
	{
		basicParsePartTests(ChronoField.HOUR_OF_DAY, 0, 23);
		CronExpressionParser.parseHourOfDaySubpart("5,1-5,1/3");
	}

	@Test
	public void parseDayOfMonthPartTest() throws ParseException
	{
		basicParsePartTests(ChronoField.DAY_OF_MONTH, 1, 31);
		CronExpressionParser.parseDayOfMonthSubpart("1,1-5,1/3,L,LW,1W");
	}

	@Test
	public void parseMonthPartTest() throws ParseException
	{
		basicParsePartTests(ChronoField.MONTH_OF_YEAR, 1, 12);
		CronExpressionParser.parseMonthSubpart("1,1-5,1/3");
	}

	@Test
	public void parseDayOfWeekPartTest() throws ParseException
	{
		basicParsePartTests(ChronoField.DAY_OF_WEEK, 1, 7);
		CronExpressionParser.parseDayOfWeekSubpart("1,1-5,1/3,L,4#2,4L");
	}

	@Test
	public void parseYearPartTest() throws ParseException
	{
		basicParsePartTests(ChronoField.YEAR, 0, 11);
		CronExpressionParser.parseYearSubpart("1,1-5,1/3");
	}

	private void basicParsePartTests(ChronoField chronoField, int minValue, int maxValue) throws ParseException
	{
		String part = "1";
		CronSubpart subpart = CronExpressionParser.parseSubpart(part, chronoField, minValue, maxValue);
		assertTrue(subpart instanceof NumbersSubpart);

		part = "1-4";
		subpart = CronExpressionParser.parseSubpart(part, chronoField, minValue, maxValue);
		assertTrue(subpart instanceof RangeNumbersSubpart);

		part = "*";
		subpart = CronExpressionParser.parseSubpart(part, chronoField, minValue, maxValue);
		assertTrue(subpart instanceof AllNumbersSubpart);

		part = "*-*";
		subpart = CronExpressionParser.parseSubpart(part, chronoField, minValue, maxValue);
		assertTrue(subpart instanceof RangeNumbersSubpart);
		RangeNumbersSubpart rnsp = (RangeNumbersSubpart) subpart;
		if (chronoField == ChronoField.DAY_OF_WEEK)
		{
			assertEquals(7, rnsp.getStartValue());
			assertEquals(6, rnsp.getEndValue());
		}
		else
		{
			assertEquals(minValue, rnsp.getStartValue());
			assertEquals(maxValue, rnsp.getEndValue());
		}
		assertEquals(1, rnsp.getIncrement());

		part = "*-*/2";
		subpart = CronExpressionParser.parseSubpart(part, chronoField, minValue, maxValue);
		assertTrue(subpart instanceof RangeNumbersSubpart);
		rnsp = (RangeNumbersSubpart) subpart;
		if (chronoField == ChronoField.DAY_OF_WEEK)
		{
			assertEquals(7, rnsp.getStartValue());
			assertEquals(6, rnsp.getEndValue());
		}
		else
		{
			assertEquals(minValue, rnsp.getStartValue());
			assertEquals(maxValue, rnsp.getEndValue());
		}
		assertEquals(2, rnsp.getIncrement());

		part = "2/2";
		subpart = CronExpressionParser.parseSubpart(part, chronoField, minValue, maxValue);
		assertTrue(subpart instanceof RangeNumbersSubpart);

		part = "5,1-5,1/3";
		subpart = CronExpressionParser.parseSubpart(part, chronoField, minValue, maxValue);
		assertTrue(subpart instanceof GroupSubpart);

		part = Integer.toString(minValue - 1);
		try
		{
			subpart = CronExpressionParser.parseSubpart(part, chronoField, minValue, maxValue);
			fail("Min range test failed!");
		}
		catch (ParseException e)
		{}

		part = Integer.toString(maxValue + 1);
		try
		{
			subpart = CronExpressionParser.parseSubpart(part, chronoField, minValue, maxValue);
			fail("Max range test failed!");
		}
		catch (ParseException e)
		{}
	}

	@Test
	public void replaceMonthValuesTest()
	{
		String subpart = "JAN FEB MAR APR MAY JUN JUL AUG SEP OCT NOV DEC";
		String s = CronExpressionParser.replaceMonthValues(subpart);
		assertEquals("1 2 3 4 5 6 7 8 9 10 11 12", s);
	}

	@Test
	public void replaceDayOfWeekValuesTest()
	{
		String subpart = "MON TUE WED THU FRI SAT SUN";
		String s = CronExpressionParser.replaceDayOfWeekValues(subpart);
		assertEquals("2 3 4 5 6 7 1", s);
	}
}
