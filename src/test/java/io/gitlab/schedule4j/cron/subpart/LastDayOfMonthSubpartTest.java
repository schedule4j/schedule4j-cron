/*
 * Created on 2010-03-03
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import static org.junit.Assert.assertEquals;
import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.util.TestHelper;

import java.text.ParseException;
import java.time.ZonedDateTime;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class LastDayOfMonthSubpartTest
{
	@Test
	public void checkTest() throws ParseException
	{
		// check lastDayOfMonth
		checkTest("2010-01-15", false);
		checkTest("2010-02-15", false);
		checkTest("2010-03-15", false);
		checkTest("2010-04-15", false);
		checkTest("2010-05-15", false);
		checkTest("2010-06-15", false);
		checkTest("2010-07-15", false);
		checkTest("2010-08-15", false);
		checkTest("2010-09-15", false);
		checkTest("2010-10-15", false);
		checkTest("2010-11-15", false);
		checkTest("2010-12-15", false);

		checkTest("2010-01-30", false);
		checkTest("2010-02-27", false);
		checkTest("2010-03-30", false);
		checkTest("2010-04-29", false);
		checkTest("2010-05-30", false);
		checkTest("2010-06-29", false);
		checkTest("2010-07-30", false);
		checkTest("2010-08-30", false);
		checkTest("2010-09-29", false);
		checkTest("2010-10-30", false);
		checkTest("2010-11-29", false);
		checkTest("2010-12-30", false);

		checkTest("2010-01-31", true);
		checkTest("2010-02-28", true);
		checkTest("2010-03-31", true);
		checkTest("2010-04-30", true);
		checkTest("2010-05-31", true);
		checkTest("2010-06-30", true);
		checkTest("2010-07-31", true);
		checkTest("2010-08-31", true);
		checkTest("2010-09-30", true);
		checkTest("2010-10-31", true);
		checkTest("2010-11-30", true);
		checkTest("2010-12-31", true);
	}

	private void checkTest(String dateStr, boolean expectedCheck) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateStr(dateStr);
		CronSubpart subpart = new LastDayOfMonthSubpart();
		boolean check = subpart.check(dateTime);
		assertEquals(expectedCheck, check);
	}

	@Test
	public void getNextTest() throws ParseException
	{
		getNextTest("2010-01-15", 31, 0);
		getNextTest("2010-02-15", 28, 0);
		getNextTest("2010-03-15", 31, 0);
		getNextTest("2010-04-15", 30, 0);
		getNextTest("2010-05-15", 31, 0);
		getNextTest("2010-06-15", 30, 0);
		getNextTest("2010-07-15", 31, 0);
		getNextTest("2010-08-15", 31, 0);
		getNextTest("2010-09-15", 30, 0);
		getNextTest("2010-10-15", 31, 0);
		getNextTest("2010-11-15", 30, 0);
		getNextTest("2010-12-15", 31, 0);

		getNextTest("2010-01-31", 28, 1);
		getNextTest("2010-02-28", 31, 1);
		getNextTest("2010-03-31", 30, 1);
		getNextTest("2010-04-30", 31, 1);
		getNextTest("2010-05-31", 30, 1);
		getNextTest("2010-06-30", 31, 1);
		getNextTest("2010-07-31", 31, 1);
		getNextTest("2010-08-31", 30, 1);
		getNextTest("2010-09-30", 31, 1);
		getNextTest("2010-10-31", 30, 1);
		getNextTest("2010-11-30", 31, 1);
		getNextTest("2010-12-31", 31, 1);
	}

	private void getNextTest(String dateStr, int expectedDay, int expectedAddCarry) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateStr(dateStr);
		CronSubpart subpart = new LastDayOfMonthSubpart();
		CronResult cronResult = subpart.getNext(dateTime);
		assertEquals(expectedDay, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}

	@Test
	public void getPreviousTest() throws ParseException
	{
		getPreviousTest("2010-01-15", 31, -1);
		getPreviousTest("2010-02-15", 31, -1);
		getPreviousTest("2010-03-15", 28, -1);
		getPreviousTest("2010-04-15", 31, -1);
		getPreviousTest("2010-05-15", 30, -1);
		getPreviousTest("2010-06-15", 31, -1);
		getPreviousTest("2010-07-15", 30, -1);
		getPreviousTest("2010-08-15", 31, -1);
		getPreviousTest("2010-09-15", 31, -1);
		getPreviousTest("2010-10-15", 30, -1);
		getPreviousTest("2010-11-15", 31, -1);
		getPreviousTest("2010-12-15", 30, -1);

		getPreviousTest("2010-01-01", 31, -1);
		getPreviousTest("2010-02-01", 31, -1);
		getPreviousTest("2010-03-01", 28, -1);
		getPreviousTest("2012-03-01", 29, -1);
		getPreviousTest("2010-04-01", 31, -1);
		getPreviousTest("2010-05-01", 30, -1);
		getPreviousTest("2010-06-01", 31, -1);
		getPreviousTest("2010-07-01", 30, -1);
		getPreviousTest("2010-08-01", 31, -1);
		getPreviousTest("2010-09-01", 31, -1);
		getPreviousTest("2010-10-01", 30, -1);
		getPreviousTest("2010-11-01", 31, -1);
		getPreviousTest("2010-12-01", 30, -1);
	}

	private void getPreviousTest(String dateStr, int expectedDay, int expectedAddCarry) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateStr(dateStr);
		CronSubpart subpart = new LastDayOfMonthSubpart();
		CronResult cronResult = subpart.getPrevious(dateTime);
		assertEquals(expectedDay, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}
}
