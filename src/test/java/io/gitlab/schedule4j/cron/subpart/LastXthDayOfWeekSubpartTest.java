/*
 * Created on 2010-03-06
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.util.TestHelper;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class LastXthDayOfWeekSubpartTest
{
	@Test
	public void checkTest() throws ParseException
	{
		checkTest("2010-01-01", 1, 25);
		checkTest("2010-02-01", 1, 22);
		checkTest("2010-03-01", 1, 29);
		checkTest("2010-04-01", 1, 26);
		checkTest("2010-05-01", 1, 31);
		checkTest("2010-06-01", 1, 28);
		checkTest("2010-07-01", 1, 26);
		checkTest("2010-08-01", 1, 30);
		checkTest("2010-09-01", 1, 27);
		checkTest("2010-10-01", 1, 25);
		checkTest("2010-11-01", 1, 29);
		checkTest("2010-12-01", 1, 27);

		checkTest("2010-01-01", 2, 26);
		checkTest("2010-02-01", 2, 23);
		checkTest("2010-03-01", 2, 30);
		checkTest("2010-04-01", 2, 27);
		checkTest("2010-05-01", 2, 25);
		checkTest("2010-06-01", 2, 29);
		checkTest("2010-07-01", 2, 27);
		checkTest("2010-08-01", 2, 31);
		checkTest("2010-09-01", 2, 28);
		checkTest("2010-10-01", 2, 26);
		checkTest("2010-11-01", 2, 30);
		checkTest("2010-12-01", 2, 28);

		checkTest("2010-01-01", 3, 27);
		checkTest("2010-02-01", 3, 24);
		checkTest("2010-03-01", 3, 31);
		checkTest("2010-04-01", 3, 28);
		checkTest("2010-05-01", 3, 26);
		checkTest("2010-06-01", 3, 30);
		checkTest("2010-07-01", 3, 28);
		checkTest("2010-08-01", 3, 25);
		checkTest("2010-09-01", 3, 29);
		checkTest("2010-10-01", 3, 27);
		checkTest("2010-11-01", 3, 24);
		checkTest("2010-12-01", 3, 29);

		checkTest("2010-01-01", 4, 28);
		checkTest("2010-02-01", 4, 25);
		checkTest("2010-03-01", 4, 25);
		checkTest("2010-04-01", 4, 29);
		checkTest("2010-05-01", 4, 27);
		checkTest("2010-06-01", 4, 24);
		checkTest("2010-07-01", 4, 29);
		checkTest("2010-08-01", 4, 26);
		checkTest("2010-09-01", 4, 30);
		checkTest("2010-10-01", 4, 28);
		checkTest("2010-11-01", 4, 25);
		checkTest("2010-12-01", 4, 30);

		checkTest("2010-01-01", 5, 29);
		checkTest("2010-02-01", 5, 26);
		checkTest("2010-03-01", 5, 26);
		checkTest("2010-04-01", 5, 30);
		checkTest("2010-05-01", 5, 28);
		checkTest("2010-06-01", 5, 25);
		checkTest("2010-07-01", 5, 30);
		checkTest("2010-08-01", 5, 27);
		checkTest("2010-09-01", 5, 24);
		checkTest("2010-10-01", 5, 29);
		checkTest("2010-11-01", 5, 26);
		checkTest("2010-12-01", 5, 31);

		checkTest("2010-01-01", 6, 30);
		checkTest("2010-02-01", 6, 27);
		checkTest("2010-03-01", 6, 27);
		checkTest("2010-04-01", 6, 24);
		checkTest("2010-05-01", 6, 29);
		checkTest("2010-06-01", 6, 26);
		checkTest("2010-07-01", 6, 31);
		checkTest("2010-08-01", 6, 28);
		checkTest("2010-09-01", 6, 25);
		checkTest("2010-10-01", 6, 30);
		checkTest("2010-11-01", 6, 27);
		checkTest("2010-12-01", 6, 25);

		checkTest("2010-01-01", 7, 31);
		checkTest("2010-02-01", 7, 28);
		checkTest("2010-03-01", 7, 28);
		checkTest("2010-04-01", 7, 25);
		checkTest("2010-05-01", 7, 30);
		checkTest("2010-06-01", 7, 27);
		checkTest("2010-07-01", 7, 25);
		checkTest("2010-08-01", 7, 29);
		checkTest("2010-09-01", 7, 26);
		checkTest("2010-10-01", 7, 31);
		checkTest("2010-11-01", 7, 28);
		checkTest("2010-12-01", 7, 26);
	}

	private void checkTest(String dateStr, int weekday, int lastDay) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateStr(dateStr);
		CronSubpart subpart = new LastXthDayOfWeekSubpart(weekday);
		int max = dateTime.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth();
		for (int i = 0; i < max; i++)
		{
			if (dateTime.getDayOfMonth() == lastDay)
			{
				assertTrue(subpart.check(dateTime));
			}
			else
			{
				assertFalse(subpart.check(dateTime));
			}
			dateTime = dateTime.plusDays(1);
		}
	}

	@Test
	public void getNextTest() throws ParseException
	{
		// MONDAY
		getNextTest("2010-01-24", 1, 25, 0);
		getNextTest("2010-01-25", 1, 22, 1);
		getNextTest("2010-03-21", 1, 29, 0);
		getNextTest("2010-03-29", 1, 26, 1);
		// TUESDAY
		getNextTest("2010-01-24", 2, 26, 0);
		getNextTest("2010-01-26", 2, 23, 1);
		getNextTest("2010-03-21", 2, 30, 0);
		getNextTest("2010-03-30", 2, 27, 1);
		// WEDNESDAY
		getNextTest("2010-01-24", 3, 27, 0);
		getNextTest("2010-01-27", 3, 24, 1);
		getNextTest("2010-03-21", 3, 31, 0);
		getNextTest("2010-03-31", 3, 28, 1);
		// THURSDAY
		getNextTest("2010-01-24", 4, 28, 0);
		getNextTest("2010-01-28", 4, 25, 1);
		getNextTest("2010-03-21", 4, 25, 0);
		getNextTest("2010-03-25", 4, 29, 1);
		// FRIDAY
		getNextTest("2010-01-24", 5, 29, 0);
		getNextTest("2010-01-29", 5, 26, 1);
		getNextTest("2010-03-21", 5, 26, 0);
		getNextTest("2010-03-26", 5, 30, 1);
		// SATURDAY
		getNextTest("2010-01-24", 6, 30, 0);
		getNextTest("2010-01-30", 6, 27, 1);
		getNextTest("2010-03-21", 6, 27, 0);
		getNextTest("2010-03-27", 6, 24, 1);
		// SUNDAY
		getNextTest("2010-01-24", 7, 31, 0);
		getNextTest("2010-01-31", 7, 28, 1);
		getNextTest("2010-03-21", 7, 28, 0);
		getNextTest("2010-03-28", 7, 25, 1);
	}

	private void getNextTest(String dateStr, int weekday, int expectedDay, int expectedAddCarry) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateStr(dateStr);
		CronSubpart subpart = new LastXthDayOfWeekSubpart(weekday);
		CronResult cronResult = subpart.getNext(dateTime);
		assertEquals(expectedDay, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}

	@Test
	public void getPreviousTest() throws ParseException
	{
		// MONDAY
		getPreviousTest("2010-01-31", 1, 25, 0);
		getPreviousTest("2010-02-01", 1, 25, -1);
		getPreviousTest("2010-03-31", 1, 29, 0);
		getPreviousTest("2010-04-01", 1, 29, -1);
		// TUESDAY
		getPreviousTest("2010-01-31", 2, 26, 0);
		getPreviousTest("2010-02-01", 2, 26, -1);
		getPreviousTest("2010-03-31", 2, 30, 0);
		getPreviousTest("2010-04-01", 2, 30, -1);
		// WEDNESDAY
		getPreviousTest("2010-01-31", 3, 27, 0);
		getPreviousTest("2010-02-01", 3, 27, -1);
		getPreviousTest("2010-03-31", 3, 24, -1);
		getPreviousTest("2010-04-01", 3, 31, -1);
		// THURSDAY
		getPreviousTest("2010-01-31", 4, 28, 0);
		getPreviousTest("2010-02-01", 4, 28, -1);
		getPreviousTest("2010-03-31", 4, 25, 0);
		getPreviousTest("2010-04-01", 4, 25, -1);
		// FRIDAY
		getPreviousTest("2010-01-31", 5, 29, 0);
		getPreviousTest("2010-02-01", 5, 29, -1);
		getPreviousTest("2010-03-31", 5, 26, 0);
		getPreviousTest("2010-04-01", 5, 26, -1);
		// SATURDAY
		getPreviousTest("2010-01-31", 6, 30, 0);
		getPreviousTest("2010-02-01", 6, 30, -1);
		getPreviousTest("2010-03-31", 6, 27, 0);
		getPreviousTest("2010-04-01", 6, 27, -1);
		// SUNDAY
		getPreviousTest("2010-01-31", 7, 27, -1);
		getPreviousTest("2010-02-01", 7, 31, -1);
		getPreviousTest("2010-03-31", 7, 28, 0);
		getPreviousTest("2010-04-01", 7, 28, -1);
	}

	private void getPreviousTest(String dateStr, int weekday, int expectedDay, int expectedAddCarry)
		throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateStr(dateStr);
		CronSubpart subpart = new LastXthDayOfWeekSubpart(weekday);
		CronResult cronResult = subpart.getPrevious(dateTime);
		assertEquals(expectedDay, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}
}
