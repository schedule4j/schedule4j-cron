/*
 * Created on 2010-03-08
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import static org.junit.Assert.assertEquals;
import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.subpart.CronSubpart;
import io.gitlab.schedule4j.cron.subpart.XthDayOfMonthSubpart;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class XthDayOfMonthSubpartTest
{
	@Test
	public void checkTest() throws ParseException
	{
		// Monday
		checkTest("2010-01-11", 1, 1, false);
		checkTest("2010-01-04", 1, 1, true);
		checkTest("2010-01-04", 1, 2, false);
		checkTest("2010-01-11", 1, 2, true);
		checkTest("2010-01-11", 1, 3, false);
		checkTest("2010-01-18", 1, 3, true);
		checkTest("2010-01-18", 1, 4, false);
		checkTest("2010-01-25", 1, 4, true);
		checkTest("2010-03-22", 1, 5, false);
		checkTest("2010-03-29", 1, 5, true);
		// Tuesday
		checkTest("2010-01-12", 2, 1, false);
		checkTest("2010-01-05", 2, 1, true);
		checkTest("2010-01-05", 2, 2, false);
		checkTest("2010-01-12", 2, 2, true);
		checkTest("2010-01-12", 2, 3, false);
		checkTest("2010-01-19", 2, 3, true);
		checkTest("2010-01-19", 2, 4, false);
		checkTest("2010-01-26", 2, 4, true);
		checkTest("2010-03-23", 2, 5, false);
		checkTest("2010-03-30", 2, 5, true);
		// Wednesday
		checkTest("2010-01-13", 3, 1, false);
		checkTest("2010-01-06", 3, 1, true);
		checkTest("2010-01-06", 3, 2, false);
		checkTest("2010-01-13", 3, 2, true);
		checkTest("2010-01-13", 3, 3, false);
		checkTest("2010-01-20", 3, 3, true);
		checkTest("2010-01-20", 3, 4, false);
		checkTest("2010-01-27", 3, 4, true);
		checkTest("2010-03-24", 3, 5, false);
		checkTest("2010-03-31", 3, 5, true);
		// Thursday
		checkTest("2010-01-14", 4, 1, false);
		checkTest("2010-01-07", 4, 1, true);
		checkTest("2010-01-07", 4, 2, false);
		checkTest("2010-01-14", 4, 2, true);
		checkTest("2010-01-14", 4, 3, false);
		checkTest("2010-01-21", 4, 3, true);
		checkTest("2010-01-21", 4, 4, false);
		checkTest("2010-01-28", 4, 4, true);
		checkTest("2010-04-22", 4, 5, false);
		checkTest("2010-04-29", 4, 5, true);
		// Friday
		checkTest("2010-01-08", 5, 1, false);
		checkTest("2010-01-01", 5, 1, true);
		checkTest("2010-01-07", 5, 2, false);
		checkTest("2010-01-08", 5, 2, true);
		checkTest("2010-01-08", 5, 3, false);
		checkTest("2010-01-15", 5, 3, true);
		checkTest("2010-01-15", 5, 4, false);
		checkTest("2010-01-22", 5, 4, true);
		checkTest("2010-01-22", 5, 5, false);
		checkTest("2010-01-29", 5, 5, true);
		// Saturday
		checkTest("2010-01-16", 6, 1, false);
		checkTest("2010-01-02", 6, 1, true);
		checkTest("2010-01-02", 6, 2, false);
		checkTest("2010-01-09", 6, 2, true);
		checkTest("2010-01-09", 6, 3, false);
		checkTest("2010-01-16", 6, 3, true);
		checkTest("2010-01-16", 6, 4, false);
		checkTest("2010-01-23", 6, 4, true);
		checkTest("2010-01-23", 6, 5, false);
		checkTest("2010-01-30", 6, 5, true);
		// Sunday
		checkTest("2010-01-02", 7, 1, false);
		checkTest("2010-01-03", 7, 1, true);
		checkTest("2010-01-02", 7, 2, false);
		checkTest("2010-01-10", 7, 2, true);
		checkTest("2010-01-10", 7, 3, false);
		checkTest("2010-01-17", 7, 3, true);
		checkTest("2010-01-17", 7, 4, false);
		checkTest("2010-01-24", 7, 4, true);
		checkTest("2010-01-24", 7, 5, false);
		checkTest("2010-01-31", 7, 5, true);
	}

	private void checkTest(String dateStr, int dayOfWeek, int count, boolean expectedCheck)
		throws ParseException
	{
		ZonedDateTime dateTime = LocalDate.parse(dateStr, DateTimeFormatter.ISO_DATE).atStartOfDay(
			ZoneId.of("Europe/Berlin"));
		CronSubpart subpart = new XthDayOfMonthSubpart(dayOfWeek, count);
		assertEquals(expectedCheck, subpart.check(dateTime));
	}

	@Test
	public void getNextTest() throws ParseException
	{
		getNextTest("2010-03-01", 4, 1, 4, 0);
		getNextTest("2010-03-01", 2, 1, 2, 0);
		getNextTest("2010-03-01", 3, 3, 17, 0);
		getNextTest("2010-03-31", 5, 1, 2, 1);
		getNextTest("2010-03-31", 7, 5, 30, 2);
	}

	private void getNextTest(String dateStr, int dayOfWeek, int count, int expected, int addCarry)
		throws ParseException
	{
		ZonedDateTime dateTime = LocalDate.parse(dateStr, DateTimeFormatter.ISO_DATE).atStartOfDay(
			ZoneId.of("Europe/Berlin"));
		CronSubpart subpart = new XthDayOfMonthSubpart(dayOfWeek, count);
		CronResult cr = subpart.getNext(dateTime);
		assertEquals(expected, cr.getNumber());
		assertEquals(addCarry, cr.getAddCarry());
	}

	@Test
	public void getPreviousTest() throws ParseException
	{
		getPreviousTest("2010-03-01", 2, 4, 23, -1);
		getPreviousTest("2010-03-01", 6, 3, 20, -1);
		getPreviousTest("2010-03-01", 4, 1, 4, -1);
		getPreviousTest("2010-03-01", 1, 2, 8, -1);
		getPreviousTest("2010-05-01", 2, 5, 30, -2);
	}

	private void getPreviousTest(String dateStr, int weekday, int count, int expected, int addCarry)
		throws ParseException
	{
		ZonedDateTime dateTime = LocalDate.parse(dateStr, DateTimeFormatter.ISO_DATE).atStartOfDay(
			ZoneId.of("Europe/Berlin"));
		CronSubpart subpart = new XthDayOfMonthSubpart(weekday, count);
		CronResult cr = subpart.getPrevious(dateTime);
		assertEquals(expected, cr.getNumber());
		assertEquals(addCarry, cr.getAddCarry());
	}
}
