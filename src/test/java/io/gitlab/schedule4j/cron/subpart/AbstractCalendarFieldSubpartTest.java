/*
 * Created on 2017-09-30
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import static org.junit.Assert.assertEquals;
import io.gitlab.schedule4j.cron.util.TestHelper;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class AbstractCalendarFieldSubpartTest
{
	@Test
	public void getMinMaxValueTest()
	{
		ZonedDateTime zdt = TestHelper.createZonedDateTimeByDateStr("2017-09-30");
		AbstractCalendarFieldSubpart subpart = new AllNumbersSubpart(ChronoField.SECOND_OF_MINUTE);
		assertEquals(0, subpart.getMinValue(zdt));
		assertEquals(59, subpart.getMaxValue(zdt));
		subpart = new AllNumbersSubpart(ChronoField.MINUTE_OF_HOUR);
		assertEquals(0, subpart.getMinValue(zdt));
		assertEquals(59, subpart.getMaxValue(zdt));
		subpart = new AllNumbersSubpart(ChronoField.HOUR_OF_DAY);
		assertEquals(0, subpart.getMinValue(zdt));
		assertEquals(23, subpart.getMaxValue(zdt));
		subpart = new AllNumbersSubpart(ChronoField.DAY_OF_WEEK);
		assertEquals(1, subpart.getMinValue(zdt));
		assertEquals(7, subpart.getMaxValue(zdt));
		subpart = new AllNumbersSubpart(ChronoField.DAY_OF_MONTH);
		assertEquals(1, subpart.getMinValue(zdt));
		assertEquals(30, subpart.getMaxValue(zdt));
		zdt = TestHelper.createZonedDateTimeByDateStr("2017-02-01");
		assertEquals(1, subpart.getMinValue(zdt));
		assertEquals(28, subpart.getMaxValue(zdt));
		subpart = new AllNumbersSubpart(ChronoField.MONTH_OF_YEAR);
		assertEquals(1, subpart.getMinValue(zdt));
		assertEquals(12, subpart.getMaxValue(zdt));
		subpart = new AllNumbersSubpart(ChronoField.YEAR);
		assertEquals(Integer.MIN_VALUE, subpart.getMinValue(zdt));
		assertEquals(Integer.MAX_VALUE, subpart.getMaxValue(zdt));
	}

	@Test(expected = UnsupportedOperationException.class)
	public void getMinValueUnsupportedChronoField()
	{
		ZonedDateTime zdt = TestHelper.createZonedDateTimeByDateStr("2017-09-30");
		AbstractCalendarFieldSubpart subpart = new AllNumbersSubpart(ChronoField.NANO_OF_DAY);
		subpart.getMinValue(zdt);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void getMaxValueUnsupportedChronoField()
	{
		ZonedDateTime zdt = TestHelper.createZonedDateTimeByDateStr("2017-09-30");
		AbstractCalendarFieldSubpart subpart = new AllNumbersSubpart(ChronoField.NANO_OF_DAY);
		subpart.getMaxValue(zdt);
	}
}
