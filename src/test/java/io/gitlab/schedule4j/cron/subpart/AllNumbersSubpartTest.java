/*
 * Created on 2010-03-06
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import static org.junit.Assert.assertEquals;
import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.util.TestHelper;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class AllNumbersSubpartTest
{
	@Test
	public void checkTest() throws ParseException
	{
		checkTest("2010-01-01T10:00:00", ChronoField.SECOND_OF_MINUTE, true);
		checkTest("2010-01-01T10:00:10", ChronoField.SECOND_OF_MINUTE, true);

		checkTest("2010-01-01T10:00:00", ChronoField.MINUTE_OF_HOUR, true);
		checkTest("2010-01-01T10:10:00", ChronoField.MINUTE_OF_HOUR, true);

		checkTest("2010-01-01T10:00:00", ChronoField.HOUR_OF_DAY, true);
		checkTest("2010-01-01T11:00:00", ChronoField.HOUR_OF_DAY, true);

		checkTest("2010-01-01T10:00:00", ChronoField.DAY_OF_MONTH, true);
		checkTest("2010-01-10T10:00:00", ChronoField.DAY_OF_MONTH, true);

		checkTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, true);
		checkTest("2010-01-10T10:00:00", ChronoField.DAY_OF_WEEK, true);

		checkTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, true);
		checkTest("2010-02-01T10:00:00", ChronoField.MONTH_OF_YEAR, true);
	}

	private void checkTest(String dateTimeStr, ChronoField chronoField, boolean expectedCheck) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateTimeStr(dateTimeStr);
		CronSubpart subpart = new AllNumbersSubpart(chronoField);
		assertEquals(expectedCheck, subpart.check(dateTime));
	}

	@Test
	public void getNextTest() throws ParseException
	{
		getNextTest("2010-01-01T10:00:00", ChronoField.SECOND_OF_MINUTE, 1, 0);
		getNextTest("2010-01-01T10:00:59", ChronoField.SECOND_OF_MINUTE, 0, 1);

		getNextTest("2010-01-01T10:00:00", ChronoField.MINUTE_OF_HOUR, 1, 0);
		getNextTest("2010-01-01T10:59:00", ChronoField.MINUTE_OF_HOUR, 0, 1);

		getNextTest("2010-01-01T10:00:00", ChronoField.HOUR_OF_DAY, 11, 0);
		getNextTest("2010-01-01T23:00:00", ChronoField.HOUR_OF_DAY, 0, 1);

		getNextTest("2010-01-01T10:00:00", ChronoField.DAY_OF_MONTH, 2, 0);
		getNextTest("2010-01-31T10:00:00", ChronoField.DAY_OF_MONTH, 1, 1);
		getNextTest("2010-02-28T10:00:00", ChronoField.DAY_OF_MONTH, 1, 1);
		getNextTest("2012-02-28T10:00:00", ChronoField.DAY_OF_MONTH, 29, 0);
		// 29 February test
		getNextTest("2012-02-29T10:00:00", ChronoField.DAY_OF_MONTH, 1, 1);
		// 28 February test
		getNextTest("2010-02-28T10:00:00", ChronoField.DAY_OF_MONTH, 1, 1);

		getNextTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, 2, 0);
		getNextTest("2010-01-02T10:00:00", ChronoField.DAY_OF_WEEK, 3, 0);
		getNextTest("2010-01-31T10:00:00", ChronoField.DAY_OF_WEEK, 1, 1);

		getNextTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, 2, 0);
		getNextTest("2010-12-01T10:00:00", ChronoField.MONTH_OF_YEAR, 1, 1);

		getNextTest("2010-01-01T10:00:00", ChronoField.YEAR, 2011, 0);
	}

	private void getNextTest(String dateTimeStr, ChronoField chronoField, int expectedDay, int expectedAddCarry)
		throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateTimeStr(dateTimeStr);
		CronSubpart subpart = new AllNumbersSubpart(chronoField);
		CronResult cronResult = subpart.getNext(dateTime);
		assertEquals(expectedDay, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}

	@Test
	public void getPreviousTest() throws ParseException
	{
		getPreviousTest("2010-01-01T10:00:00", ChronoField.SECOND_OF_MINUTE, 59, -1);
		getPreviousTest("2010-01-01T10:00:59", ChronoField.SECOND_OF_MINUTE, 58, 0);

		getPreviousTest("2010-01-01T10:00:00", ChronoField.MINUTE_OF_HOUR, 59, -1);
		getPreviousTest("2010-01-01T10:59:00", ChronoField.MINUTE_OF_HOUR, 58, 0);

		getPreviousTest("2010-01-01T10:00:00", ChronoField.HOUR_OF_DAY, 9, 0);
		getPreviousTest("2010-01-01T00:00:00", ChronoField.HOUR_OF_DAY, 23, -1);

		getPreviousTest("2010-01-01T10:00:00", ChronoField.DAY_OF_MONTH, 31, -1);
		getPreviousTest("2010-01-31T10:00:00", ChronoField.DAY_OF_MONTH, 30, 0);
		getPreviousTest("2010-03-01T10:00:00", ChronoField.DAY_OF_MONTH, 28, -1);
		getPreviousTest("2012-03-01T10:00:00", ChronoField.DAY_OF_MONTH, 29, -1);
		getPreviousTest("2012-02-29T10:00:00", ChronoField.DAY_OF_MONTH, 28, 0);

		getPreviousTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, 31, -1);
		getPreviousTest("2010-01-02T10:00:00", ChronoField.DAY_OF_WEEK, 1, 0);
		getPreviousTest("2010-01-31T10:00:00", ChronoField.DAY_OF_WEEK, 30, 0);
		getPreviousTest("2010-02-01T10:00:00", ChronoField.DAY_OF_WEEK, 31, -1);

		getPreviousTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, 12, -1);
		getPreviousTest("2010-12-01T10:00:00", ChronoField.MONTH_OF_YEAR, 11, 0);

		getPreviousTest("2010-01-01T10:00:00", ChronoField.YEAR, 2009, 0);
	}

	private void getPreviousTest(String dateTimeStr, ChronoField chronoField, int expectedDay, int expectedAddCarry)
		throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateTimeStr(dateTimeStr);
		CronSubpart subpart = new AllNumbersSubpart(chronoField);
		CronResult cronResult = subpart.getPrevious(dateTime);
		assertEquals(expectedDay, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}
}
