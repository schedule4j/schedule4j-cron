/*
 * Created on 2015-07-19
 * 
 * Copyright 2015 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.util.TestHelper;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class LastWeekdayOfMonthSubpartTest
{
	@Test
	public void checkTest() throws ParseException
	{
		checkTest("2010-01-01", 29);
		checkTest("2010-02-01", 26);
		checkTest("2010-03-01", 31);
		checkTest("2010-04-01", 30);
		checkTest("2010-05-01", 31);
		checkTest("2010-06-01", 30);
		checkTest("2010-07-01", 30);
		checkTest("2010-08-01", 31);
		checkTest("2010-09-01", 30);
		checkTest("2010-10-01", 29);
		checkTest("2010-11-01", 30);
		checkTest("2010-12-01", 31);
	}

	private void checkTest(String dateStr, int lastWeekday) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateStr(dateStr);
		CronSubpart subpart = new LastWeekdayOfMonthSubpart();
		int max = dateTime.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth();
		for (int i = 0; i < max; i++)
		{
			if (dateTime.getDayOfMonth() == lastWeekday)
			{
				assertTrue(subpart.check(dateTime));
			}
			else
			{
				assertFalse(subpart.check(dateTime));
			}
			dateTime = dateTime.plusDays(1);
		}
	}

	@Test
	public void getNextTest() throws ParseException
	{
		getNextTest("2010-01-24", 29, 0);
		getNextTest("2010-01-30", 26, 1);
		getNextTest("2010-03-21", 31, 0);
		getNextTest("2010-03-31", 30, 1);
	}

	private void getNextTest(String dateStr, int expectedDay, int expectedAddCarry) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateStr(dateStr);
		CronSubpart subpart = new LastWeekdayOfMonthSubpart();
		CronResult cronResult = subpart.getNext(dateTime);
		assertEquals(expectedDay, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}

	@Test
	public void getPreviousTest() throws ParseException
	{
		getPreviousTest("2010-01-31", 29, 0);
		getPreviousTest("2010-02-01", 29, -1);
		getPreviousTest("2010-03-31", 26, -1);
		getPreviousTest("2010-02-28", 26, 0);
	}

	private void getPreviousTest(String dateStr, int expectedDay, int expectedAddCarry) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateStr(dateStr);
		CronSubpart subpart = new LastWeekdayOfMonthSubpart();
		CronResult cronResult = subpart.getPrevious(dateTime);
		assertEquals(expectedDay, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}
}
