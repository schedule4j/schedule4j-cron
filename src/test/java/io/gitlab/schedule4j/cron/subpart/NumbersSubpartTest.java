/*
 * Created on 2010-03-07
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import static org.junit.Assert.assertEquals;
import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.util.TestHelper;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class NumbersSubpartTest
{
	@Test
	public void checkTest() throws ParseException
	{
		checkTest("2010-01-01T10:00:00", ChronoField.SECOND_OF_MINUTE, true, 0, 10);
		checkTest("2010-01-01T10:00:59", ChronoField.SECOND_OF_MINUTE, false, 0, 10);

		checkTest("2010-01-01T10:00:00", ChronoField.MINUTE_OF_HOUR, true, 0, 10);
		checkTest("2010-01-01T10:17:00", ChronoField.MINUTE_OF_HOUR, false, 0, 10);

		checkTest("2010-01-01T10:00:00", ChronoField.HOUR_OF_DAY, true, 0, 10);
		checkTest("2010-01-01T11:00:00", ChronoField.HOUR_OF_DAY, false, 0, 10);

		checkTest("2010-01-01T10:00:00", ChronoField.DAY_OF_MONTH, true, 1, 3);
		checkTest("2010-01-10T10:00:00", ChronoField.DAY_OF_MONTH, false, 1, 3);

		checkTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, true, 2, 5);
		checkTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, false, 2, 6);

		checkTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, true, 1, 3);
		checkTest("2010-02-01T10:00:00", ChronoField.MONTH_OF_YEAR, false, 1, 3);
	}

	private void checkTest(String dateTimeStr, ChronoField chronoField, boolean expectedCheck, int firstNumber,
		int... numbers) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateTimeStr(dateTimeStr);
		NumbersSubpart subpart = new NumbersSubpart(chronoField, firstNumber);
		for (int i = 0; i < numbers.length; i++)
		{
			subpart.addNumber(numbers[i]);
		}
		assertEquals(expectedCheck, subpart.check(dateTime));
	}

	@Test
	public void getNextTest() throws ParseException
	{
		getNextTest("2010-01-01T10:00:00", ChronoField.SECOND_OF_MINUTE, 1, 0, 0, 1);
		getNextTest("2010-01-01T10:00:59", ChronoField.SECOND_OF_MINUTE, 0, 1, 0, 1);

		getNextTest("2010-01-01T10:00:00", ChronoField.MINUTE_OF_HOUR, 1, 0, 0, 1);
		getNextTest("2010-01-01T10:59:00", ChronoField.MINUTE_OF_HOUR, 0, 1, 0, 1);

		getNextTest("2010-01-01T10:00:00", ChronoField.HOUR_OF_DAY, 11, 0, 0, 11);
		getNextTest("2010-01-01T23:00:00", ChronoField.HOUR_OF_DAY, 0, 1, 0, 11);

		getNextTest("2010-01-01T10:00:00", ChronoField.DAY_OF_MONTH, 2, 0, 1, 2);
		getNextTest("2010-01-31T10:00:00", ChronoField.DAY_OF_MONTH, 1, 1, 1, 2);
		getNextTest("2010-02-28T10:00:00", ChronoField.DAY_OF_MONTH, 28, 1, 28, 29);
		getNextTest("2012-02-28T10:00:00", ChronoField.DAY_OF_MONTH, 29, 0, 28, 29);
		getNextTest("2010-02-28T10:00:00", ChronoField.DAY_OF_MONTH, 29, 1, 29);
		// February 29 on a non leap year
		getNextTest("2010-01-29T10:00:00", ChronoField.DAY_OF_MONTH, 29, 2, 29);

		getNextTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, 4, 0, 1, 4);// FRIDAY
		getNextTest("2010-01-03T10:00:00", ChronoField.DAY_OF_WEEK, 4, 0, 1, 4);// SUNDAY
		getNextTest("2010-01-31T10:00:00", ChronoField.DAY_OF_WEEK, 1, 1, 1, 4);// SUNDAY

		getNextTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, 2, 0, 1, 2);
		getNextTest("2010-12-01T10:00:00", ChronoField.MONTH_OF_YEAR, 1, 1, 1, 2);

		getNextTest("2010-01-01T10:00:00", ChronoField.YEAR, 2011, 0, 2010, 2011);
	}

	private void getNextTest(String dateTimeStr, ChronoField chronoField, int expectedDay, int expectedAddCarry,
		int firstNumber, int... numbers) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateTimeStr(dateTimeStr);
		NumbersSubpart subpart = new NumbersSubpart(chronoField, firstNumber);
		for (int i = 0; i < numbers.length; i++)
		{
			subpart.addNumber(numbers[i]);
		}
		CronResult cronResult = subpart.getNext(dateTime);
		assertEquals(expectedDay, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}

	@Test
	public void getPreviousTest() throws ParseException
	{
		getPreviousTest("2010-01-01T10:00:00", ChronoField.SECOND_OF_MINUTE, 59, -1, 58, 59);
		getPreviousTest("2010-01-01T10:00:59", ChronoField.SECOND_OF_MINUTE, 58, 0, 58, 59);
		getPreviousTest("2010-01-01T10:00:58", ChronoField.SECOND_OF_MINUTE, 59, -1, 58, 59);

		getPreviousTest("2010-01-01T10:00:00", ChronoField.MINUTE_OF_HOUR, 59, -1, 58, 59);
		getPreviousTest("2010-01-01T10:59:00", ChronoField.MINUTE_OF_HOUR, 58, 0, 58, 59);
		getPreviousTest("2010-01-01T10:58:00", ChronoField.MINUTE_OF_HOUR, 59, -1, 58, 59);

		getPreviousTest("2010-01-01T10:00:00", ChronoField.HOUR_OF_DAY, 8, 0, 8, 23);
		getPreviousTest("2010-01-01T00:00:00", ChronoField.HOUR_OF_DAY, 23, -1, 8, 23);
		getPreviousTest("2010-01-01T07:00:00", ChronoField.HOUR_OF_DAY, 23, -1, 8, 23);

		getPreviousTest("2010-01-01T10:00:00", ChronoField.DAY_OF_MONTH, 30, -1, 30);
		getPreviousTest("2010-01-31T10:00:00", ChronoField.DAY_OF_MONTH, 30, 0, 30);
		getPreviousTest("2010-03-01T10:00:00", ChronoField.DAY_OF_MONTH, 28, -1, 28, 29);
		getPreviousTest("2012-03-01T10:00:00", ChronoField.DAY_OF_MONTH, 29, -1, 28, 29);
		getPreviousTest("2012-02-29T10:00:00", ChronoField.DAY_OF_MONTH, 28, 0, 28, 29);
		// February 29 on a non leap year
		getPreviousTest("2010-03-01T00:00:00", ChronoField.DAY_OF_MONTH, 31, -2, 31);

		getPreviousTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, 31, -1, 1, 4);// FRIDAY
		getPreviousTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, 30, -1, 1, 3);// FRIDAY
		getPreviousTest("2010-01-04T10:00:00", ChronoField.DAY_OF_WEEK, 1, 0, 1, 5);// MONDAY
		getPreviousTest("2010-01-04T10:00:00", ChronoField.DAY_OF_WEEK, 31, -1, 1, 4);// MONDAY
		getPreviousTest("2010-01-31T10:00:00", ChronoField.DAY_OF_WEEK, 28, 0, 1, 4);// SUNDAY
		getPreviousTest("2010-02-01T10:00:00", ChronoField.DAY_OF_WEEK, 28, -1, 1, 4);// MONDAY

		getPreviousTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, 11, -1, 10, 11);
		getPreviousTest("2010-11-01T10:00:00", ChronoField.MONTH_OF_YEAR, 12, -1, 11, 12);

		getPreviousTest("2010-01-01T10:00:00", ChronoField.YEAR, 2008, 0, 2008);
	}

	private void getPreviousTest(String dateTimeStr, ChronoField chronoField, int expectedDay, int expectedAddCarry,
		int firstNumber, int... numbers) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateTimeStr(dateTimeStr);
		NumbersSubpart subpart = new NumbersSubpart(chronoField, firstNumber);
		for (int i = 0; i < numbers.length; i++)
		{
			subpart.addNumber(numbers[i]);
		}
		CronResult cronResult = subpart.getPrevious(dateTime);
		assertEquals(expectedDay, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}
}
