/*
 * Created on 2010-02-20
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.util.TestHelper;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class RangeNumbersSubpartTest
{
	@Test(expected = IllegalArgumentException.class)
	public void yearCalendarFieldTest()
	{
		new RangeNumbersSubpart(ChronoField.YEAR, 2010, 2000, 1);
	}

	@Test
	public void constructorTest()
	{
		RangeNumbersSubpart subPart = null;
		subPart = new RangeNumbersSubpart(ChronoField.SECOND_OF_MINUTE, 0, 59, 1);
		assertEquals(60, subPart.getAllowedNumbers().size());
		assertEquals(0, subPart.getAllowedNumbers().first().intValue());
		assertEquals(59, subPart.getAllowedNumbers().last().intValue());
		subPart = new RangeNumbersSubpart(ChronoField.SECOND_OF_MINUTE, 20, 50, 5);
		assertEquals(7, subPart.getAllowedNumbers().size());
		assertEquals(20, subPart.getAllowedNumbers().first().intValue());
		assertEquals(50, subPart.getAllowedNumbers().last().intValue());
		assertTrue(subPart.getAllowedNumbers().contains(25));
		assertTrue(subPart.getAllowedNumbers().contains(30));
		assertTrue(subPart.getAllowedNumbers().contains(35));
		assertTrue(subPart.getAllowedNumbers().contains(40));
		assertTrue(subPart.getAllowedNumbers().contains(45));
		subPart = new RangeNumbersSubpart(ChronoField.SECOND_OF_MINUTE, 50, 20, 5);
		assertEquals(7, subPart.getAllowedNumbers().size());
		assertEquals(0, subPart.getAllowedNumbers().first().intValue());
		assertEquals(55, subPart.getAllowedNumbers().last().intValue());
		assertTrue(subPart.getAllowedNumbers().contains(5));
		assertTrue(subPart.getAllowedNumbers().contains(10));
		assertTrue(subPart.getAllowedNumbers().contains(15));
		assertTrue(subPart.getAllowedNumbers().contains(20));
		assertTrue(subPart.getAllowedNumbers().contains(50));

		subPart = new RangeNumbersSubpart(ChronoField.MINUTE_OF_HOUR, 0, 59, 1);
		assertEquals(60, subPart.getAllowedNumbers().size());
		assertEquals(0, subPart.getAllowedNumbers().first().intValue());
		assertEquals(59, subPart.getAllowedNumbers().last().intValue());
		subPart = new RangeNumbersSubpart(ChronoField.MINUTE_OF_HOUR, 10, 30, 5);
		assertEquals(5, subPart.getAllowedNumbers().size());
		assertEquals(10, subPart.getAllowedNumbers().first().intValue());
		assertEquals(30, subPart.getAllowedNumbers().last().intValue());
		subPart = new RangeNumbersSubpart(ChronoField.MINUTE_OF_HOUR, 50, 10, 5);
		assertEquals(5, subPart.getAllowedNumbers().size());
		assertEquals(0, subPart.getAllowedNumbers().first().intValue());
		assertEquals(55, subPart.getAllowedNumbers().last().intValue());

		subPart = new RangeNumbersSubpart(ChronoField.HOUR_OF_DAY, 0, 23, 1);
		assertEquals(24, subPart.getAllowedNumbers().size());
		assertEquals(0, subPart.getAllowedNumbers().first().intValue());
		assertEquals(23, subPart.getAllowedNumbers().last().intValue());
		subPart = new RangeNumbersSubpart(ChronoField.HOUR_OF_DAY, 0, 23, 4);
		assertEquals(6, subPart.getAllowedNumbers().size());
		assertEquals(0, subPart.getAllowedNumbers().first().intValue());
		assertEquals(20, subPart.getAllowedNumbers().last().intValue());
		subPart = new RangeNumbersSubpart(ChronoField.HOUR_OF_DAY, 18, 6, 4);
		assertEquals(4, subPart.getAllowedNumbers().size());
		assertEquals(2, subPart.getAllowedNumbers().first().intValue());
		assertEquals(22, subPart.getAllowedNumbers().last().intValue());

		subPart = new RangeNumbersSubpart(ChronoField.DAY_OF_WEEK, 1, 7, 1);
		assertEquals(7, subPart.getAllowedNumbers().size());
		assertEquals(1, subPart.getAllowedNumbers().first().intValue());
		assertEquals(7, subPart.getAllowedNumbers().last().intValue());
		subPart = new RangeNumbersSubpart(ChronoField.DAY_OF_WEEK, 1, 7, 3);
		assertEquals(3, subPart.getAllowedNumbers().size());
		assertEquals(1, subPart.getAllowedNumbers().first().intValue());
		assertEquals(7, subPart.getAllowedNumbers().last().intValue());
		subPart = new RangeNumbersSubpart(ChronoField.DAY_OF_WEEK, 6, 2, 2);
		assertEquals(2, subPart.getAllowedNumbers().size());
		assertEquals(1, subPart.getAllowedNumbers().first().intValue());
		assertEquals(6, subPart.getAllowedNumbers().last().intValue());

		subPart = new RangeNumbersSubpart(ChronoField.DAY_OF_MONTH, 1, 31, 1);
		assertEquals(31, subPart.getAllowedNumbers().size());
		assertEquals(1, subPart.getAllowedNumbers().first().intValue());
		assertEquals(31, subPart.getAllowedNumbers().last().intValue());
		subPart = new RangeNumbersSubpart(ChronoField.DAY_OF_MONTH, 1, 31, 5);
		assertEquals(7, subPart.getAllowedNumbers().size());
		assertEquals(1, subPart.getAllowedNumbers().first().intValue());
		assertEquals(31, subPart.getAllowedNumbers().last().intValue());
		subPart = new RangeNumbersSubpart(ChronoField.DAY_OF_MONTH, 21, 11, 5);
		assertEquals(5, subPart.getAllowedNumbers().size());
		assertEquals(5, subPart.getAllowedNumbers().first().intValue());
		assertEquals(31, subPart.getAllowedNumbers().last().intValue());
		assertTrue(subPart.getAllowedNumbers().contains(5));
		assertTrue(subPart.getAllowedNumbers().contains(10));
		assertTrue(subPart.getAllowedNumbers().contains(26));

		subPart = new RangeNumbersSubpart(ChronoField.MONTH_OF_YEAR, 1, 12, 1);
		assertEquals(12, subPart.getAllowedNumbers().size());
		assertEquals(1, subPart.getAllowedNumbers().first().intValue());
		assertEquals(12, subPart.getAllowedNumbers().last().intValue());
		subPart = new RangeNumbersSubpart(ChronoField.MONTH_OF_YEAR, 1, 12, 3);
		assertEquals(4, subPart.getAllowedNumbers().size());
		assertEquals(1, subPart.getAllowedNumbers().first().intValue());
		assertEquals(10, subPart.getAllowedNumbers().last().intValue());
		subPart = new RangeNumbersSubpart(ChronoField.MONTH_OF_YEAR, 7, 3, 3);
		assertEquals(3, subPart.getAllowedNumbers().size());
		assertEquals(1, subPart.getAllowedNumbers().first().intValue());
		assertEquals(10, subPart.getAllowedNumbers().last().intValue());

		subPart = new RangeNumbersSubpart(ChronoField.YEAR, 2000, 2020, 1);
		assertEquals(1, subPart.getAllowedNumbers().size());
		assertEquals(2000, subPart.getAllowedNumbers().first().intValue());
		assertEquals(2000, subPart.getAllowedNumbers().last().intValue());
	}

	@Test
	public void getNextForYearTest()
	{
		RangeNumbersSubpart subPart = null;
		CronResult cr = null;
		subPart = new RangeNumbersSubpart(ChronoField.YEAR, 2000, 2100, 1);
		cr = subPart.getNext(2005, Integer.MIN_VALUE, Integer.MAX_VALUE);
		assertEquals(2006, cr.getNumber());
		assertEquals(0, cr.getAddCarry());
		cr = subPart.getNext(2099, Integer.MIN_VALUE, Integer.MAX_VALUE);
		assertEquals(2100, cr.getNumber());
		assertEquals(0, cr.getAddCarry());
		cr = subPart.getNext(2100, Integer.MIN_VALUE, Integer.MAX_VALUE);
		assertNull(cr);

		subPart = new RangeNumbersSubpart(ChronoField.YEAR, 2000, 2100, 7);
		cr = subPart.getNext(2005, Integer.MIN_VALUE, Integer.MAX_VALUE);
		assertEquals(2007, cr.getNumber());
		assertEquals(0, cr.getAddCarry());
		cr = subPart.getNext(2091, Integer.MIN_VALUE, Integer.MAX_VALUE);
		assertEquals(2098, cr.getNumber());
		assertEquals(0, cr.getAddCarry());
		cr = subPart.getNext(2099, Integer.MIN_VALUE, Integer.MAX_VALUE);
		assertNull(cr);
	}

	@Test
	public void getPreviousForYearTest()
	{
		RangeNumbersSubpart subPart = null;
		CronResult cr = null;
		subPart = new RangeNumbersSubpart(ChronoField.YEAR, 2000, 2100, 1);
		cr = subPart.getPrevious(2005, Integer.MIN_VALUE, Integer.MAX_VALUE);
		assertEquals(2004, cr.getNumber());
		assertEquals(0, cr.getAddCarry());
		cr = subPart.getPrevious(2199, Integer.MIN_VALUE, Integer.MAX_VALUE);
		assertEquals(2100, cr.getNumber());
		assertEquals(0, cr.getAddCarry());
		cr = subPart.getPrevious(2000, Integer.MIN_VALUE, Integer.MAX_VALUE);
		assertNull(cr);

		subPart = new RangeNumbersSubpart(ChronoField.YEAR, 2000, 2100, 7);
		cr = subPart.getPrevious(2008, Integer.MIN_VALUE, Integer.MAX_VALUE);
		assertEquals(2007, cr.getNumber());
		assertEquals(0, cr.getAddCarry());
		cr = subPart.getPrevious(2099, Integer.MIN_VALUE, Integer.MAX_VALUE);
		assertEquals(2098, cr.getNumber());
		assertEquals(0, cr.getAddCarry());
		cr = subPart.getPrevious(2000, Integer.MIN_VALUE, Integer.MAX_VALUE);
		assertNull(cr);
	}

	@Test
	public void checkDateTest() throws ParseException
	{
		checkDateTest("2010-01-01T10:00:00", ChronoField.SECOND_OF_MINUTE, 0, 59, 1, true);
		checkDateTest("2010-01-01T10:00:00", ChronoField.SECOND_OF_MINUTE, 10, 59, 1, false);
		checkDateTest("2010-01-01T10:00:20", ChronoField.SECOND_OF_MINUTE, 10, 59, 2, true);
		checkDateTest("2010-01-01T10:00:21", ChronoField.SECOND_OF_MINUTE, 10, 59, 2, false);

		checkDateTest("2010-01-01T10:00:00", ChronoField.MINUTE_OF_HOUR, 0, 59, 1, true);
		checkDateTest("2010-01-01T10:00:00", ChronoField.MINUTE_OF_HOUR, 10, 59, 1, false);
		checkDateTest("2010-01-01T10:20:00", ChronoField.MINUTE_OF_HOUR, 10, 59, 2, true);
		checkDateTest("2010-01-01T10:23:00", ChronoField.MINUTE_OF_HOUR, 10, 59, 2, false);

		checkDateTest("2010-01-01T10:00:00", ChronoField.HOUR_OF_DAY, 0, 23, 1, true);
		checkDateTest("2010-01-01T10:00:00", ChronoField.HOUR_OF_DAY, 11, 23, 1, false);
		checkDateTest("2010-01-01T13:00:00", ChronoField.HOUR_OF_DAY, 11, 23, 2, true);
		checkDateTest("2010-01-01T14:00:00", ChronoField.HOUR_OF_DAY, 11, 23, 2, false);

		checkDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_MONTH, 1, 31, 1, true);
		checkDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_MONTH, 10, 31, 1, false);
		checkDateTest("2010-01-12T10:00:00", ChronoField.DAY_OF_MONTH, 10, 31, 2, true);
		checkDateTest("2010-01-15T10:00:00", ChronoField.DAY_OF_MONTH, 10, 31, 2, false);

		checkDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, 1, 7, 1, true);
		checkDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, 1, 4, 1, false);
		checkDateTest("2010-01-02T10:00:00", ChronoField.DAY_OF_WEEK, 1, 7, 2, false);
		checkDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, 1, 7, 2, true);

		checkDateTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, 1, 12, 1, true);
		checkDateTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, 2, 12, 1, false);
		checkDateTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, 1, 12, 2, true);
		checkDateTest("2010-02-01T10:00:00", ChronoField.MONTH_OF_YEAR, 1, 12, 2, false);

		checkDateTest("2010-01-01T10:00:00", ChronoField.YEAR, 2010, 2011, 1, true);
		checkDateTest("2012-01-01T10:00:00", ChronoField.YEAR, 2010, 2011, 1, false);
		checkDateTest("2012-01-01T10:00:00", ChronoField.YEAR, 2010, 2015, 2, true);
		checkDateTest("2013-01-01T10:00:00", ChronoField.YEAR, 2010, 2015, 2, false);

		checkDateTest("2010-01-01T10:00:00", ChronoField.YEAR, 1900, 2100, 1, true);
		checkDateTest("2011-01-31T10:00:00", ChronoField.YEAR, 1900, 2100, 1, true);
		checkDateTest("2011-01-31T10:00:00", ChronoField.YEAR, 1900, 2100, 2, false);
	}

	private void checkDateTest(String dateTimeStr, ChronoField chronoField, int startValue, int endValue, int increment,
		boolean expectedCheck) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateTimeStr(dateTimeStr);
		CronSubpart subpart = new RangeNumbersSubpart(chronoField, startValue, endValue, increment);
		assertEquals(expectedCheck, subpart.check(dateTime));
	}

	@Test
	public void getNextByDateTest() throws ParseException
	{
		getNextByDateTest("2010-01-01T10:00:00", ChronoField.SECOND_OF_MINUTE, 0, 59, 1, 1, 0);
		getNextByDateTest("2010-01-01T10:00:59", ChronoField.SECOND_OF_MINUTE, 0, 59, 1, 0, 1);
		getNextByDateTest("2010-01-01T10:00:00", ChronoField.SECOND_OF_MINUTE, 30, 10, 1, 1, 0);
		getNextByDateTest("2010-01-01T10:00:59", ChronoField.SECOND_OF_MINUTE, 30, 10, 1, 0, 1);

		getNextByDateTest("2010-01-01T10:00:00", ChronoField.MINUTE_OF_HOUR, 0, 59, 1, 1, 0);
		getNextByDateTest("2010-01-01T10:59:00", ChronoField.MINUTE_OF_HOUR, 0, 59, 1, 0, 1);
		getNextByDateTest("2010-01-01T10:00:00", ChronoField.MINUTE_OF_HOUR, 30, 10, 1, 1, 0);
		getNextByDateTest("2010-01-01T10:59:00", ChronoField.MINUTE_OF_HOUR, 30, 10, 1, 0, 1);

		getNextByDateTest("2010-01-01T10:00:00", ChronoField.HOUR_OF_DAY, 0, 23, 1, 11, 0);
		getNextByDateTest("2010-01-01T23:00:00", ChronoField.HOUR_OF_DAY, 0, 23, 1, 0, 1);
		getNextByDateTest("2010-01-01T10:00:00", ChronoField.HOUR_OF_DAY, 23, 12, 1, 11, 0);
		getNextByDateTest("2010-01-01T23:00:00", ChronoField.HOUR_OF_DAY, 23, 12, 1, 0, 1);

		getNextByDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_MONTH, 1, 31, 1, 2, 0);
		getNextByDateTest("2010-01-31T10:00:00", ChronoField.DAY_OF_MONTH, 1, 31, 1, 1, 1);
		getNextByDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_MONTH, 25, 10, 1, 2, 0);
		getNextByDateTest("2010-01-31T10:00:00", ChronoField.DAY_OF_MONTH, 25, 10, 1, 1, 1);

		getNextByDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, 1, 7, 1, 2, 0);
		getNextByDateTest("2010-01-31T10:00:00", ChronoField.DAY_OF_WEEK, 1, 7, 1, 1, 1);
		getNextByDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, 6, 3, 1, 2, 0);
		getNextByDateTest("2010-01-31T10:00:00", ChronoField.DAY_OF_WEEK, 6, 3, 1, 1, 1);

		getNextByDateTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, 1, 12, 1, 2, 0);
		getNextByDateTest("2010-12-31T10:00:00", ChronoField.MONTH_OF_YEAR, 1, 12, 1, 1, 1);
		getNextByDateTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, 8, 4, 1, 2, 0);
		getNextByDateTest("2010-12-31T10:00:00", ChronoField.MONTH_OF_YEAR, 8, 4, 1, 1, 1);

		getNextByDateTest("2010-01-01T10:00:00", ChronoField.YEAR, 1900, 2100, 1, 2011, 0);
		getNextByDateTest("2011-01-31T10:00:00", ChronoField.YEAR, 1900, 2100, 1, 2012, 0);
		getNextByDateTest("2010-01-31T10:00:00", ChronoField.YEAR, 1900, 2100, 2, 2012, 0);
	}

	private void getNextByDateTest(String dateTimeStr, ChronoField chronoField, int startValue, int endValue,
		int increment, int expectedNumber, int expectedAddCarry) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateTimeStr(dateTimeStr);
		CronSubpart subpart = new RangeNumbersSubpart(chronoField, startValue, endValue, increment);
		CronResult cronResult = subpart.getNext(dateTime);
		assertEquals(expectedNumber, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}

	@Test
	public void getPreviousByDateTest() throws ParseException
	{
		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.SECOND_OF_MINUTE, 0, 59, 1, 59, -1);
		getPreviousByDateTest("2010-01-01T10:00:59", ChronoField.SECOND_OF_MINUTE, 0, 59, 1, 58, 0);
		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.SECOND_OF_MINUTE, 30, 10, 1, 59, -1);
		getPreviousByDateTest("2010-01-01T10:00:59", ChronoField.SECOND_OF_MINUTE, 30, 10, 1, 58, 0);

		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.MINUTE_OF_HOUR, 0, 59, 1, 59, -1);
		getPreviousByDateTest("2010-01-01T10:59:00", ChronoField.MINUTE_OF_HOUR, 0, 59, 1, 58, 0);
		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.MINUTE_OF_HOUR, 30, 10, 1, 59, -1);
		getPreviousByDateTest("2010-01-01T10:59:00", ChronoField.MINUTE_OF_HOUR, 30, 10, 1, 58, 0);

		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.HOUR_OF_DAY, 0, 23, 1, 9, 0);
		getPreviousByDateTest("2010-01-01T00:00:00", ChronoField.HOUR_OF_DAY, 0, 23, 1, 23, -1);
		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.HOUR_OF_DAY, 23, 12, 1, 9, 0);
		getPreviousByDateTest("2010-01-01T00:00:00", ChronoField.HOUR_OF_DAY, 23, 12, 1, 23, -1);

		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_MONTH, 1, 31, 1, 31, -1);
		getPreviousByDateTest("2010-01-31T10:00:00", ChronoField.DAY_OF_MONTH, 1, 31, 1, 30, 0);
		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_MONTH, 25, 10, 1, 31, -1);
		getPreviousByDateTest("2010-01-31T10:00:00", ChronoField.DAY_OF_MONTH, 25, 10, 1, 30, 0);

		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, 1, 7, 1, 31, -1);
		getPreviousByDateTest("2010-01-31T10:00:00", ChronoField.DAY_OF_WEEK, 1, 7, 1, 30, 0);
		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.DAY_OF_WEEK, 6, 3, 1, 30, -1);
		getPreviousByDateTest("2010-01-31T10:00:00", ChronoField.DAY_OF_WEEK, 6, 3, 1, 30, 0);

		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, 1, 12, 1, 12, -1);
		getPreviousByDateTest("2010-12-31T10:00:00", ChronoField.MONTH_OF_YEAR, 1, 12, 1, 11, 0);
		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.MONTH_OF_YEAR, 8, 4, 1, 12, -1);
		getPreviousByDateTest("2010-12-31T10:00:00", ChronoField.MONTH_OF_YEAR, 8, 4, 1, 11, 0);

		getPreviousByDateTest("2010-01-01T10:00:00", ChronoField.YEAR, 1900, 2100, 1, 2009, 0);
		getPreviousByDateTest("2011-01-31T10:00:00", ChronoField.YEAR, 1900, 2100, 1, 2010, 0);
		getPreviousByDateTest("2012-01-31T10:00:00", ChronoField.YEAR, 1900, 2100, 2, 2010, 0);
	}

	private void getPreviousByDateTest(String dateTimeStr, ChronoField chronoField, int startValue, int endValue,
		int increment, int expectedNumber, int expectedAddCarry) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateTimeStr(dateTimeStr);
		CronSubpart subpart = new RangeNumbersSubpart(chronoField, startValue, endValue, increment);
		CronResult cronResult = subpart.getPrevious(dateTime);
		assertEquals(expectedNumber, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}
}
