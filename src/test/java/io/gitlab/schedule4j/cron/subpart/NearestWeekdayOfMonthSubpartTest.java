/*
 * Created on 2011-04-13
 *
 * Copyright 2011 Dirk Buchhorn.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

import io.gitlab.schedule4j.cron.CronResult;

/**
 * @author Dirk Buchhorn
 */
public class NearestWeekdayOfMonthSubpartTest
{
	@Test(expected = IllegalArgumentException.class)
	public void constructorDayOfMonthLessThanTest()
	{
		new NearestWeekdayOfMonthSubpart(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructorDayOfMonthGreaterThanTest()
	{
		new NearestWeekdayOfMonthSubpart(32);
	}

	@Test
	public void calculateDayOfMonthTest()
	{
		// last day of month is a Sunday
		NearestWeekdayOfMonthSubpart subpart = new NearestWeekdayOfMonthSubpart(31);
		ZonedDateTime dateTime = LocalDate.parse("2010-01-01", DateTimeFormatter.ISO_DATE)
			.atStartOfDay(ZoneId.of("Europe/Berlin"));
		int dom = subpart.calculateDayOfMonth(dateTime, 0);
		assertEquals(29, dom);
		// first day of month is a Sunday
		subpart = new NearestWeekdayOfMonthSubpart(1);
		dateTime = LocalDate.parse("2010-08-01", DateTimeFormatter.ISO_DATE)
			.atStartOfDay(ZoneId.of("Europe/Berlin"));
		dom = subpart.calculateDayOfMonth(dateTime, 0);
		assertEquals(2, dom);
		// first day of month is a Saturday
		subpart = new NearestWeekdayOfMonthSubpart(1);
		dateTime = LocalDate.parse("2010-05-01", DateTimeFormatter.ISO_DATE)
			.atStartOfDay(ZoneId.of("Europe/Berlin"));
		dom = subpart.calculateDayOfMonth(dateTime, 0);
		assertEquals(3, dom);
		// day of month (>1) is a Saturday
		subpart = new NearestWeekdayOfMonthSubpart(8);
		dateTime = LocalDate.parse("2010-05-01", DateTimeFormatter.ISO_DATE)
			.atStartOfDay(ZoneId.of("Europe/Berlin"));
		dom = subpart.calculateDayOfMonth(dateTime, 0);
		assertEquals(7, dom);
		// dayOfMonth=31 month=February
		subpart = new NearestWeekdayOfMonthSubpart(31);
		dateTime = LocalDate.parse("2010-02-28", DateTimeFormatter.ISO_DATE)
			.atStartOfDay(ZoneId.of("Europe/Berlin"));
		dom = subpart.calculateDayOfMonth(dateTime, 0);
		assertEquals(26, dom);
	}

	@Test
	public void checkTest() throws ParseException
	{
		checkTest("2010-03-01", 1, true);
		checkTest("2010-03-02", 2, true);
		checkTest("2010-03-01", 2, false);
		checkTest("2010-03-02", 1, false);
		checkTest("2010-03-12", 13, true);
		checkTest("2010-03-15", 14, true);
		checkTest("2010-01-01", 1, true);
		checkTest("2010-01-01", 2, true);
		checkTest("2010-01-04", 3, true);
		checkTest("2010-01-04", 4, true);
		checkTest("2010-01-11", 10, true);
		checkTest("2010-01-11", 11, true);
		checkTest("2010-01-02", 1, false);
		checkTest("2010-01-04", 1, false);
	}

	private void checkTest(String dateStr, int dayOfMonth, boolean expectedCheck) throws ParseException
	{
		ZonedDateTime dateTime = LocalDate.parse(dateStr, DateTimeFormatter.ISO_DATE)
			.atStartOfDay(ZoneId.of("Europe/Berlin"));
		CronSubpart subpart = new NearestWeekdayOfMonthSubpart(dayOfMonth);
		assertEquals(expectedCheck, subpart.check(dateTime));
	}

	@Test
	public void getNextTest() throws ParseException
	{
		getNextTest("2010-03-01", 1, 1, 1);
		getNextTest("2010-03-01", 2, 2, 0);
		getNextTest("2010-03-01", 3, 3, 0);
		getNextTest("2010-03-01", 4, 4, 0);
		getNextTest("2010-03-01", 5, 5, 0);
		getNextTest("2010-03-01", 6, 5, 0);
		getNextTest("2010-03-01", 7, 8, 0);
		getNextTest("2010-03-01", 13, 12, 0);
		getNextTest("2010-03-01", 14, 15, 0);
		getNextTest("2010-04-01", 1, 3, 1);
		getNextTest("2010-04-01", 2, 2, 0);
		getNextTest("2010-04-01", 3, 2, 0);
		getNextTest("2010-04-01", 4, 5, 0);
		getNextTest("2010-04-01", 5, 5, 0);
	}

	private void getNextTest(String dateStr, int weekDayOfMonth, int expectedDay, int addCarry)
		throws ParseException
	{
		ZonedDateTime dateTime = LocalDate.parse(dateStr, DateTimeFormatter.ISO_DATE)
			.atStartOfDay(ZoneId.of("Europe/Berlin"));
		CronSubpart subpart = new NearestWeekdayOfMonthSubpart(weekDayOfMonth);
		CronResult cr = subpart.getNext(dateTime);
		assertEquals(expectedDay, cr.getNumber());
		assertEquals(addCarry, cr.getAddCarry());
	}

	@Test
	public void getPreviousTest() throws ParseException
	{
		getPreviousTest("2010-03-01", 1, 1, -1);
		getPreviousTest("2010-03-01", 2, 2, -1);
		getPreviousTest("2010-03-01", 3, 3, -1);
		getPreviousTest("2010-03-01", 4, 4, -1);
		getPreviousTest("2010-03-01", 5, 5, -1);
		getPreviousTest("2010-03-01", 6, 5, -1);
		getPreviousTest("2010-03-01", 7, 8, -1);
		getPreviousTest("2010-04-01", 1, 1, -1);
		getPreviousTest("2010-04-01", 2, 2, -1);
		getPreviousTest("2010-04-01", 3, 3, -1);
		getPreviousTest("2010-04-01", 4, 4, -1);
		getPreviousTest("2010-04-01", 5, 5, -1);
		getPreviousTest("2010-04-01", 6, 5, -1);
		getPreviousTest("2010-04-01", 7, 8, -1);
	}

	private void getPreviousTest(String dateStr, int weekDayOfMonth, int expectedDay, int addCarry)
		throws ParseException
	{
		ZonedDateTime dateTime = LocalDate.parse(dateStr, DateTimeFormatter.ISO_DATE)
			.atStartOfDay(ZoneId.of("Europe/Berlin"));
		CronSubpart subpart = new NearestWeekdayOfMonthSubpart(weekDayOfMonth);
		CronResult cr = subpart.getPrevious(dateTime);
		assertEquals(expectedDay, cr.getNumber());
		assertEquals(addCarry, cr.getAddCarry());
	}
}
