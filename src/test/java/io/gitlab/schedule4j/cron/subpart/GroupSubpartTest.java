/*
 * Created on 2010-03-21
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.subpart;

import static org.junit.Assert.assertEquals;
import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.util.TestHelper;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class GroupSubpartTest
{
	@Test
	public void addTest()
	{
		// test optimizes add
		GroupSubpart groupSubpart = new GroupSubpart();
		assertEquals(0, groupSubpart.size());

		NumbersSubpart numbersSubpart = new NumbersSubpart(ChronoField.SECOND_OF_MINUTE, 0);
		groupSubpart.add(numbersSubpart);
		assertEquals(1, groupSubpart.size());

		RangeNumbersSubpart rangeSubpart = new RangeNumbersSubpart(ChronoField.SECOND_OF_MINUTE, 10, 20, 2);
		groupSubpart.add(rangeSubpart);
		assertEquals(2, groupSubpart.size());
	}

	@Test
	public void addOptimizedTest()
	{
		// test optimizes add
		GroupSubpart groupSubpart = new GroupSubpart();
		assertEquals(0, groupSubpart.size());

		NumbersSubpart numbersSubpart = new NumbersSubpart(ChronoField.SECOND_OF_MINUTE, 0);
		groupSubpart.add(numbersSubpart);
		assertEquals(1, groupSubpart.size());
		numbersSubpart = new NumbersSubpart(ChronoField.SECOND_OF_MINUTE, 10);
		groupSubpart.add(numbersSubpart);
		assertEquals(1, groupSubpart.size());
	}

	@Test
	public void checkTest() throws ParseException
	{
		// test optimizes add
		GroupSubpart groupSubpart = new GroupSubpart();
		NumbersSubpart numbersSubpart = new NumbersSubpart(ChronoField.SECOND_OF_MINUTE, 0);
		groupSubpart.add(numbersSubpart);
		RangeNumbersSubpart rangeSubpart = new RangeNumbersSubpart(ChronoField.SECOND_OF_MINUTE, 10, 20, 2);
		groupSubpart.add(rangeSubpart);

		assertEquals(true, groupSubpart.check(createZonedDateTime("2010-03-21T00:00:00")));
		assertEquals(false, groupSubpart.check(createZonedDateTime("2010-03-21T00:00:01")));
		assertEquals(true, groupSubpart.check(createZonedDateTime("2010-03-21T00:00:10")));
		assertEquals(true, groupSubpart.check(createZonedDateTime("2010-03-21T00:00:12")));
		assertEquals(true, groupSubpart.check(createZonedDateTime("2010-03-21T00:00:20")));
		assertEquals(false, groupSubpart.check(createZonedDateTime("2010-03-21T00:00:21")));
	}

	@Test
	public void getNextTest() throws ParseException
	{
		GroupSubpart groupSubpart = new GroupSubpart();
		NumbersSubpart numbersSubpart = new NumbersSubpart(ChronoField.SECOND_OF_MINUTE, 0);
		groupSubpart.add(numbersSubpart);
		RangeNumbersSubpart rangeSubpart = new RangeNumbersSubpart(ChronoField.SECOND_OF_MINUTE, 10, 20, 2);
		groupSubpart.add(rangeSubpart);

		ZonedDateTime dateTime = createZonedDateTime("2010-03-21T00:00:00");
		CronResult cronResult = groupSubpart.getNext(dateTime);
		assertEquals(10, cronResult.getNumber());
		assertEquals(0, cronResult.getAddCarry());

		dateTime = createZonedDateTime("2010-03-21T00:00:10");
		cronResult = groupSubpart.getNext(dateTime);
		assertEquals(12, cronResult.getNumber());
		assertEquals(0, cronResult.getAddCarry());

		dateTime = createZonedDateTime("2010-03-21T00:00:20");
		cronResult = groupSubpart.getNext(dateTime);
		assertEquals(0, cronResult.getNumber());
		assertEquals(1, cronResult.getAddCarry());
	}

	@Test
	public void getPreviousTest() throws ParseException
	{
		GroupSubpart groupSubpart = new GroupSubpart();
		NumbersSubpart numbersSubpart = new NumbersSubpart(ChronoField.SECOND_OF_MINUTE, 0);
		groupSubpart.add(numbersSubpart);
		RangeNumbersSubpart rangeSubpart = new RangeNumbersSubpart(ChronoField.SECOND_OF_MINUTE, 10, 20, 2);
		groupSubpart.add(rangeSubpart);

		ZonedDateTime dateTime = createZonedDateTime("2010-03-21T00:00:20");
		CronResult cronResult = groupSubpart.getPrevious(dateTime);
		assertEquals(18, cronResult.getNumber());
		assertEquals(0, cronResult.getAddCarry());

		dateTime = createZonedDateTime("2010-03-21T00:00:10");
		cronResult = groupSubpart.getPrevious(dateTime);
		assertEquals(0, cronResult.getNumber());
		assertEquals(0, cronResult.getAddCarry());

		dateTime = createZonedDateTime("2010-03-21T00:00:00");
		cronResult = groupSubpart.getPrevious(dateTime);
		assertEquals(20, cronResult.getNumber());
		assertEquals(-1, cronResult.getAddCarry());
	}

	@Test
	public void rangeNumbersTest() throws ParseException
	{
		GroupSubpart groupSubpart = new GroupSubpart();
		RangeNumbersSubpart rangeNumbersSubpart = new RangeNumbersSubpart(ChronoField.HOUR_OF_DAY, 5, 10, 1);
		RangeNumbersSubpart rangeNumbersSubpart2 = new RangeNumbersSubpart(ChronoField.HOUR_OF_DAY, 0, 23, 9);
		groupSubpart.add(rangeNumbersSubpart);
		groupSubpart.add(rangeNumbersSubpart2);

		ZonedDateTime dateTime = createZonedDateTime("2010-03-21T00:00:00");
		CronResult cronResult = groupSubpart.getNext(dateTime);
		assertEquals(5, cronResult.getNumber());
		assertEquals(0, cronResult.getAddCarry());

		dateTime = createZonedDateTime("2010-03-21T05:00:00");
		cronResult = groupSubpart.getNext(dateTime);
		assertEquals(6, cronResult.getNumber());
		assertEquals(0, cronResult.getAddCarry());

		dateTime = createZonedDateTime("2010-03-21T12:00:00");
		cronResult = groupSubpart.getNext(dateTime);
		assertEquals(18, cronResult.getNumber());
		assertEquals(0, cronResult.getAddCarry());
	}

	private ZonedDateTime createZonedDateTime(String dateTimeStr) throws ParseException
	{
		ZonedDateTime dateTime = TestHelper.createZonedDateTimeByDateTimeStr(dateTimeStr);
		return dateTime;
	}
}
