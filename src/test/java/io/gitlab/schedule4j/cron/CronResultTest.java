/*
 * Created on 2010-02-21
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class CronResultTest
{
	@Test
	public void testAfter()
	{
		testAfter(1, 0, 1, 0, false);
		testAfter(1, 0, 1, 1, false);
		testAfter(1, 0, 2, 0, false);
		testAfter(1, 0, 2, 1, false);

		testAfter(1, 1, 1, 0, true);
		testAfter(1, 1, 1, 1, false);
		testAfter(1, 1, 2, 0, true);
		testAfter(1, 1, 2, 1, false);

		testAfter(2, 0, 1, 0, true);
		testAfter(2, 0, 1, 1, false);
		testAfter(2, 0, 2, 0, false);
		testAfter(2, 0, 2, 1, false);

		testAfter(2, 1, 1, 0, true);
		testAfter(2, 1, 1, 1, true);
		testAfter(2, 1, 2, 0, true);
		testAfter(2, 1, 2, 1, false);
	}

	private void testAfter(int value1, int addCarry1, int value2, int addCarry2, boolean expectedAfter)
	{
		CronResult cronValue1 = new CronResult(value1, addCarry1);
		CronResult cronValue2 = new CronResult(value2, addCarry2);
		assertEquals(expectedAfter, cronValue1.after(cronValue2));
	}

	@Test
	public void testBefore()
	{
		testBefore(1, 0, 1, 0, false);
		testBefore(1, 0, 1, -1, false);
		testBefore(1, 0, 2, 0, true);
		testBefore(1, 0, 2, -1, false);

		testBefore(1, -1, 1, 0, true);
		testBefore(1, -1, 1, -1, false);
		testBefore(1, -1, 2, 0, true);
		testBefore(1, -1, 2, -1, true);

		testBefore(2, 0, 1, 0, false);
		testBefore(2, 0, 1, -1, false);
		testBefore(2, 0, 2, 0, false);
		testBefore(2, 0, 2, -1, false);

		testBefore(2, -1, 1, 0, true);
		testBefore(2, -1, 1, -1, false);
		testBefore(2, -1, 2, 0, true);
		testBefore(2, -1, 2, -1, false);
	}

	@Test
	public void toStringTest()
	{
		CronResult cronResult = new CronResult(5, 1);
		String s = cronResult.toString();
		assertEquals("CronResult [number=5, addCarry=1]", s);
	}

	private void testBefore(int value1, int addCarry1, int value2, int addCarry2, boolean expectedBefore)
	{
		CronResult cronValue1 = new CronResult(value1, addCarry1);
		CronResult cronValue2 = new CronResult(value2, addCarry2);
		assertEquals(expectedBefore, cronValue1.before(cronValue2));
	}
}
