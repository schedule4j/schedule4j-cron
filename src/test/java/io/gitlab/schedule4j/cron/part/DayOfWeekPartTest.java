/*
 * Created on 2010-03-07
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.part;

import static org.junit.Assert.assertEquals;
import io.gitlab.schedule4j.cron.CronResult;
import io.gitlab.schedule4j.cron.part.DayOfWeekPart;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class DayOfWeekPartTest
{
	@Test
	public void correctToDayOfMonthTest() throws ParseException
	{
		correctToDayOfMonthTest("2010-01-01", 1, -1, 21, -1);
		correctToDayOfMonthTest("2010-01-01", 2, -1, 22, -1);
		correctToDayOfMonthTest("2010-01-01", 3, -1, 23, -1);
		correctToDayOfMonthTest("2010-01-01", 4, -1, 24, -1);
		correctToDayOfMonthTest("2010-01-01", 5, -1, 25, -1);
		correctToDayOfMonthTest("2010-01-01", 6, -1, 26, -1);
		correctToDayOfMonthTest("2010-01-01", 7, -1, 27, -1);

		correctToDayOfMonthTest("2010-01-01", 1, 0, 28, -1);
		correctToDayOfMonthTest("2010-01-01", 2, 0, 29, -1);
		correctToDayOfMonthTest("2010-01-01", 3, 0, 30, -1);
		correctToDayOfMonthTest("2010-01-01", 4, 0, 31, -1);
		correctToDayOfMonthTest("2010-01-01", 5, 0, 1, 0);
		correctToDayOfMonthTest("2010-01-01", 6, 0, 2, 0);
		correctToDayOfMonthTest("2010-01-01", 7, 0, 3, 0);

		correctToDayOfMonthTest("2010-01-01", 1, 1, 4, 0);
		correctToDayOfMonthTest("2010-01-01", 2, 1, 5, 0);
		correctToDayOfMonthTest("2010-01-01", 3, 1, 6, 0);
		correctToDayOfMonthTest("2010-01-01", 4, 1, 7, 0);
		correctToDayOfMonthTest("2010-01-01", 5, 1, 8, 0);
		correctToDayOfMonthTest("2010-01-01", 6, 1, 9, 0);
		correctToDayOfMonthTest("2010-01-01", 7, 1, 10, 0);

		correctToDayOfMonthTest("2010-01-25", 1, 1, 1, 1);
		correctToDayOfMonthTest("2010-01-25", 2, 1, 2, 1);
		correctToDayOfMonthTest("2010-01-25", 3, 1, 3, 1);
		correctToDayOfMonthTest("2010-01-25", 4, 1, 4, 1);
		correctToDayOfMonthTest("2010-01-25", 5, 1, 5, 1);
		correctToDayOfMonthTest("2010-01-25", 6, 1, 6, 1);
		correctToDayOfMonthTest("2010-01-25", 7, 1, 7, 1);
	}

	public void correctToDayOfMonthTest(String dateStr, int number, int addCarry, int expectedNumber,
		int expectedAddCarry) throws ParseException
	{
		ZonedDateTime dateTime = LocalDate.parse(dateStr, DateTimeFormatter.ISO_DATE).atStartOfDay(
			ZoneId.of("Europe/Berlin"));
		CronResult cronResult = new CronResult(number, addCarry);
		cronResult = DayOfWeekPart.correctToDayOfMonth(cronResult, dateTime);
		assertEquals(expectedNumber, cronResult.getNumber());
		assertEquals(expectedAddCarry, cronResult.getAddCarry());
	}
}
