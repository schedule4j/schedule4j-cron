/*
 * Created on 2010-03-21
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.part;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import io.gitlab.schedule4j.cron.subpart.NumbersSubpart;
import io.gitlab.schedule4j.cron.subpart.RangeNumbersSubpart;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class HourPartTest extends CronPartTest
{
	@Override
	@Test
	public void testCheck()
	{
		HourPart part = createPart();
		ZonedDateTime dateTime = createZonedDateTime("2010-01-31T01:00:00");
		assertTrue(part.check(dateTime));

		dateTime = createZonedDateTime("2010-01-31T10:00:00");
		assertFalse(part.check(dateTime));

		dateTime = createZonedDateTime("2010-01-31T12:00:00");
		assertTrue(part.check(dateTime));
	}

	@Override
	@Test
	public void testCalculateNext()
	{
		HourPart part = createPart();
		ZonedDateTime dateTime = createZonedDateTime("2010-12-31T00:00:00");
		ZonedDateTime dt = part.calculateNext(dateTime);
		assertNotNull(dt);
		assertEquals("2010-12-31T01:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		dt = part.calculateNext(dt);
		assertNotNull(dt);
		assertEquals("2010-12-31T12:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		dt = part.calculateNext(dt);
		assertNotNull(dt);
		assertEquals("2010-12-31T23:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		dt = part.calculateNext(dt);
		assertNotNull(dt);
		assertEquals("2011-01-01T01:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		// test on a daylight saving day (+1 hour)
		RangeNumbersSubpart subpart = new RangeNumbersSubpart(ChronoField.HOUR_OF_DAY, 0, 7, 2);
		part = new HourPart(subpart);
		dateTime = createZonedDateTime("2014-03-30T00:30:00");
		dt = part.calculateNext(dateTime);
		// time moved forward from 2 to 3 - this value is removed later in another class
		assertEquals("2014-03-30T03:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculateNext(dt);
		assertEquals("2014-03-30T04:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dateTime = createZonedDateTime("2014-03-30T06:30:00");
		dt = part.calculateNext(dateTime);
		assertEquals("2014-03-31T00:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		// test on a daylight saving day (-1 hour)
		subpart = new RangeNumbersSubpart(ChronoField.HOUR_OF_DAY, 0, 7, 1);
		part = new HourPart(subpart);
		dateTime = createZonedDateTime("2014-10-26T00:30:00");
		dt = part.calculateNext(dateTime);
		assertEquals("2014-10-26T01:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculateNext(dt);
		assertEquals("2014-10-26T02:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculateNext(dt);
		// time moved backward from 3 to 2 - we get 2 o'clock twice
		assertEquals("2014-10-26T02:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculateNext(dt);
		assertEquals("2014-10-26T03:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dateTime = createZonedDateTime("2014-10-26T07:30:00");
		dt = part.calculateNext(dateTime);
		assertEquals("2014-10-27T00:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
	}

	@Override
	@Test
	public void testCalculatePrevious()
	{
		HourPart part = createPart();
		ZonedDateTime dateTime = createZonedDateTime("2010-01-01T23:00:00");
		ZonedDateTime dt = part.calculatePrevious(dateTime);
		assertNotNull(dt);
		assertEquals("2010-01-01T12:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		dt = part.calculatePrevious(dt);
		assertNotNull(dt);
		assertEquals("2010-01-01T01:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		dt = part.calculatePrevious(dt);
		assertNotNull(dt);
		assertEquals("2009-12-31T23:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		dt = part.calculatePrevious(dt);
		assertNotNull(dt);
		assertEquals("2009-12-31T12:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		// test on a daylight saving day (+1 hour)
		RangeNumbersSubpart subpart = new RangeNumbersSubpart(ChronoField.HOUR_OF_DAY, 0, 7, 2);
		part = new HourPart(subpart);
		dateTime = createZonedDateTime("2014-03-31T00:00:00");
		dt = part.calculatePrevious(dateTime);
		assertEquals("2014-03-30T06:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculatePrevious(dt);
		assertEquals("2014-03-30T04:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculatePrevious(dt);
		// time moved forward from 2 to 3 - this value is removed later in another class
		assertEquals("2014-03-30T01:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculatePrevious(dt);
		assertEquals("2014-03-30T00:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		// test on a daylight saving day (-1 hour)
		subpart = new RangeNumbersSubpart(ChronoField.HOUR_OF_DAY, 0, 7, 1);
		part = new HourPart(subpart);
		dateTime = createZonedDateTime("2014-10-27T00:00:00");
		dt = part.calculatePrevious(dateTime);
		assertEquals("2014-10-26T07:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dateTime = createZonedDateTime("2014-10-26T04:00:00");
		dt = part.calculatePrevious(dateTime);
		assertEquals("2014-10-26T03:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculatePrevious(dt);
		assertEquals("2014-10-26T02:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculatePrevious(dt);
		// time moved backward from 3 to 2 - we get 2 o'clock twice
		assertEquals("2014-10-26T02:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculatePrevious(dt);
		assertEquals("2014-10-26T01:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
	}

	private HourPart createPart()
	{
		NumbersSubpart subpart = new NumbersSubpart(ChronoField.HOUR_OF_DAY, 1);
		subpart.addNumber(12);
		subpart.addNumber(23);
		HourPart part = new HourPart(subpart);
		return part;
	}
}
