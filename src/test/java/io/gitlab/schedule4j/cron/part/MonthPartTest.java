/*
 * Created on 2017-09-30
 * 
 * Copyright 2017 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.part;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import io.gitlab.schedule4j.cron.subpart.NumbersSubpart;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class MonthPartTest extends CronPartTest
{
	@Override
	@Test
	public void testCheck()
	{
		MonthPart part = createPart();
		ZonedDateTime dateTime = createZonedDateTime("2010-02-10T01:00:00");
		assertTrue(part.check(dateTime));

		dateTime = createZonedDateTime("2010-05-10T01:00:00");
		assertTrue(part.check(dateTime));

		dateTime = createZonedDateTime("2010-06-10T01:00:00");
		assertTrue(part.check(dateTime));

		dateTime = createZonedDateTime("2010-10-10T01:00:00");
		assertTrue(part.check(dateTime));

		dateTime = createZonedDateTime("2010-01-31T01:00:00");
		assertFalse(part.check(dateTime));
	}

	@Override
	@Test
	public void testCalculateNext()
	{
		MonthPart part = createPart();
		ZonedDateTime dateTime = createZonedDateTime("2010-01-10T01:00:00");
		ZonedDateTime dt = part.calculateNext(dateTime);
		assertNotNull(dt);
		assertEquals("2010-02-01T00:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculateNext(dt);
		assertNotNull(dt);
		assertEquals("2010-05-01T00:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculateNext(dt);
		assertNotNull(dt);
		assertEquals("2010-06-01T00:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculateNext(dt);
		assertNotNull(dt);
		assertEquals("2010-10-01T00:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculateNext(dt);
		assertNotNull(dt);
		assertEquals("2011-02-01T00:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
	}

	@Override
	@Test
	public void testCalculatePrevious()
	{
		MonthPart part = createPart();
		ZonedDateTime dateTime = createZonedDateTime("2010-12-10T01:00:00");
		ZonedDateTime dt = part.calculatePrevious(dateTime);
		assertNotNull(dt);
		assertEquals("2010-10-31T23:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculatePrevious(dt);
		assertNotNull(dt);
		assertEquals("2010-06-30T23:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculatePrevious(dt);
		assertNotNull(dt);
		assertEquals("2010-05-31T23:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculatePrevious(dt);
		assertNotNull(dt);
		assertEquals("2010-02-28T23:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		dt = part.calculatePrevious(dt);
		assertNotNull(dt);
		assertEquals("2009-10-31T23:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
	}

	private MonthPart createPart()
	{
		NumbersSubpart subpart = new NumbersSubpart(ChronoField.MONTH_OF_YEAR, 2);
		subpart.addNumber(5);
		subpart.addNumber(6);
		subpart.addNumber(10);
		MonthPart part = new MonthPart(subpart);
		return part;
	}
}
