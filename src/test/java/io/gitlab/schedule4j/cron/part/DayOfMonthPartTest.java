/*
 * Created on 2010-03-21
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.part;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import io.gitlab.schedule4j.cron.part.DayOfMonthPart;
import io.gitlab.schedule4j.cron.subpart.NumbersSubpart;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class DayOfMonthPartTest extends CronPartTest
{
	@Override
	@Test
	public void testCheck()
	{
		DayOfMonthPart part = createPart();
		ZonedDateTime dateTime = createZonedDateTime("2010-01-01T00:00:00");
		assertTrue(part.check(dateTime));

		dateTime = createZonedDateTime("2010-01-10T00:00:00");
		assertFalse(part.check(dateTime));

		dateTime = createZonedDateTime("2010-01-28T00:00:00");
		assertTrue(part.check(dateTime));
	}

	@Override
	@Test
	public void testCalculateNext()
	{
		DayOfMonthPart part = createPart();
		ZonedDateTime dateTime = createZonedDateTime("2010-01-01T00:00:00");
		ZonedDateTime dt = part.calculateNext(dateTime);
		assertNotNull(dt);
		assertEquals("2010-01-15T00:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		dt = part.calculateNext(dt);
		assertNotNull(dt);
		assertEquals("2010-01-28T00:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		dt = part.calculateNext(dt);
		assertNotNull(dt);
		assertEquals("2010-02-01T00:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		// test year switch
		dateTime = createZonedDateTime("2010-12-31T00:00:00");
		dt = part.calculateNext(dateTime);
		assertNotNull(dt);
		assertEquals("2011-01-01T00:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
	}

	@Override
	@Test
	public void testCalculatePrevious()
	{
		DayOfMonthPart part = createPart();
		ZonedDateTime dateTime = createZonedDateTime("2010-02-01T00:00:00");
		ZonedDateTime dt = part.calculatePrevious(dateTime);
		assertNotNull(dt);
		assertEquals("2010-01-28T23:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		dt = part.calculatePrevious(dt);
		assertNotNull(dt);
		assertEquals("2010-01-15T23:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		dt = part.calculatePrevious(dt);
		assertNotNull(dt);
		assertEquals("2010-01-01T23:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		// test year switch
		dt = part.calculatePrevious(dt);
		assertNotNull(dt);
		assertEquals("2009-12-28T23:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
	}

	private DayOfMonthPart createPart()
	{
		NumbersSubpart subpart = new NumbersSubpart(ChronoField.DAY_OF_MONTH, 1);
		subpart.addNumber(15);
		subpart.addNumber(28);
		DayOfMonthPart part = new DayOfMonthPart(subpart);
		return part;
	}
}
