/*
 * Created on 2010-03-30
 *
 * Copyright 2010 Dirk Buchhorn.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.part;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import io.gitlab.schedule4j.cron.part.YearPart;
import io.gitlab.schedule4j.cron.subpart.NumbersSubpart;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class YearPartTest extends CronPartTest
{
	@Override
	@Test
	public void testCheck()
	{
		YearPart part = createPart();
		ZonedDateTime dateTime = createZonedDateTime("2010-01-31T23:59:01");
		assertTrue(part.check(dateTime));

		dateTime = createZonedDateTime("2011-01-31T23:59:31");
		assertFalse(part.check(dateTime));

		dateTime = createZonedDateTime("2012-01-31T23:59:59");
		assertTrue(part.check(dateTime));
	}

	@Override
	@Test
	public void testCalculateNext()
	{
		YearPart part = createPart();
		ZonedDateTime dateTime = createZonedDateTime("2010-12-31T23:59:00");
		ZonedDateTime dt = part.calculateNext(dateTime);
		assertNotNull(dt);
		assertEquals("2012-01-01T00:00:00", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		dt = part.calculateNext(dt);
		assertNull(dt);
	}

	@Override
	@Test
	public void testCalculatePrevious()
	{
		YearPart part = createPart();
		ZonedDateTime dateTime = createZonedDateTime("2012-12-31T23:59:00");
		ZonedDateTime dt = part.calculatePrevious(dateTime);
		assertNotNull(dt);
		assertEquals("2010-12-31T23:59:59", dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

		dt = part.calculatePrevious(dt);
		assertNull(dt);
	}

	private YearPart createPart()
	{
		NumbersSubpart subpart = new NumbersSubpart(ChronoField.YEAR, 2010);
		subpart.addNumber(2012);
		YearPart part = new YearPart(subpart);
		return part;
	}
}
