/*
 * Created on 2010-03-21
 * 
 * Copyright 2010 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron.part;

import io.gitlab.schedule4j.cron.util.TestHelper;

import java.time.ZonedDateTime;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public abstract class CronPartTest
{
	@Test
	public abstract void testCheck();

	@Test
	public abstract void testCalculateNext();

	@Test
	public abstract void testCalculatePrevious();

	protected ZonedDateTime createZonedDateTime(String dateStr)
	{
		return TestHelper.createZonedDateTimeByDateTimeStr(dateStr);
	}
}
