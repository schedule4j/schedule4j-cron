/*
 * Created on 2018-04-27
 * 
 * Copyright 2018 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.schedule4j.cron;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.ZonedDateTime;

import org.junit.Test;

/**
 * @author Dirk Buchhorn
 */
public class ZonedDateTimeHolderTest
{
	@Test
	public void test()
	{
		ZonedDateTime zdt = ZonedDateTime.now();
		ZonedDateTimeHolder zdth = new ZonedDateTimeHolder(zdt);
		assertNotNull(zdth.getZonedDateTime());
		assertEquals(zdt, zdth.getZonedDateTime());
		zdt = zdt.plusHours(1);
		zdth.setZonedDateTime(zdt);
		assertEquals(zdt, zdth.getZonedDateTime());
		zdth.setZonedDateTime(null);
		assertNull(zdth.getZonedDateTime());
	}
}
